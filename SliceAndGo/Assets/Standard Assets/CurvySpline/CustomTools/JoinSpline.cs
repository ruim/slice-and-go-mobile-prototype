﻿using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;

[HelpURL("wiki.kwalee.local")]
public class SplineFollower : MonoBehaviour
{
    [SerializeField] CurvySpline m_spline;
    [SerializeField] float m_smoothness = 5;

    SplineController m_controller;
    CurvySpline m_originalSpline;
    System.Action m_onSwitched;
    float m_targetPosition;
    bool m_isOriginallyBackward;
    bool m_isOnBridge;


    void Start()
    {
        if (m_spline != null)
            JoinSpline(m_spline);
        else
        {
            m_controller = GetComponent<SplineController>();
            if (m_controller != null && m_controller.Spline != null)
                JoinSpline(m_controller.Spline);
        }
    }

    public void JoinSpline(CurvySpline spline, System.Action onSwitched = null)
    {
        JoinSpline(spline, -1, onSwitched);
    }

    public void JoinSpline(CurvySpline spline, float minPointTF, float maxPointTF, System.Action onSwitched = null)
    {
        float nearestTF = spline.GetNearestPointTF(transform.position, Space.World);
        nearestTF = Mathf.Clamp(nearestTF, minPointTF, maxPointTF);
        JoinSpline(spline, nearestTF, onSwitched);
    }

    public void JoinSpline(CurvySpline spline, float pointTF, System.Action onSwitched = null)
    {
        m_onSwitched = onSwitched;

        if (m_isOnBridge && m_controller != null)
        {
            if (m_controller.Spline != null)
                Destroy(m_controller.Spline.gameObject);

            m_controller.OnEndReached.RemoveListener(ReachedToSpline);
            m_controller.MovementDirection = m_isOriginallyBackward ? MovementDirection.Backward : MovementDirection.Forward;
        }


        if (m_controller == null)
        {
            m_controller = GetComponent<SplineController>();
            if (m_controller == null)
            {
                m_controller = gameObject.AddComponent<SplineController>();
                m_controller.Speed = 5;
            }
        }

        m_controller.enabled = false;
        m_controller.Spline = null;
        m_originalSpline = spline;


        // Finding the closest point and best angle on the target spline and
        //float nearestTF = spline.GetNearestPointTF(transform.position, Space.World);
        float nearestTF = (pointTF >= 0) ? pointTF : spline.GetNearestPointTF(transform.position, Space.World);
        Vector3 closestPoint = spline.Interpolate(nearestTF, Space.World);
        Vector3 tangent = spline.GetTangent(nearestTF, Space.World);
        if (m_controller.MovementDirection == MovementDirection.Backward)
        {
            tangent = -tangent;
            m_controller.MovementDirection = MovementDirection.Forward;
            m_isOriginallyBackward = true;
        }

        m_targetPosition = nearestTF;


        // Setting up the bridge spline
        CurvySpline bridgeSpline = CurvySpline.Create();
        bridgeSpline.name = string.Format("- Bridge Spline to [{0}]", spline.name);
        bridgeSpline.Interpolation = CurvyInterpolation.Bezier;
        bridgeSpline.Add(transform.position);
        bridgeSpline.Add(closestPoint);

        bridgeSpline.ControlPointsList[0].AutoHandles = false;
        bridgeSpline.ControlPointsList[0].HandleOut = transform.forward * m_smoothness;

        bridgeSpline.ControlPointsList[1].AutoHandles = false;
        bridgeSpline.ControlPointsList[1].HandleIn = -tangent * m_smoothness;


        // Hooking up the controller with the bridge spline
        m_controller.Position = 0;
        m_controller.Spline = bridgeSpline;
        m_controller.OnEndReached.AddListener(ReachedToSpline);
        m_controller.enabled = true;
        m_isOnBridge = true;
    }


    private void ReachedToSpline(CurvySplineMoveEventArgs arg0)
    {
        Destroy(m_controller.Spline.gameObject);

        m_controller.Position = m_targetPosition;
        m_controller.Spline = m_originalSpline;
        if (m_isOriginallyBackward)
            m_controller.MovementDirection = MovementDirection.Backward;

        m_controller.OnEndReached.RemoveListener(ReachedToSpline);
        m_isOnBridge = false;

        m_onSwitched?.Invoke();
    }
}