﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SplinePopulator))]
public class SplinePopulatorEditor : Editor
{
    SerializedProperty m_curvySpline;
    SerializedProperty m_populateRoot;
    SerializedProperty m_splineSeparation;
    SerializedProperty m_spacing;
    SerializedProperty m_groundCollisionLayer;
    SerializedProperty m_startPrefab;
    SerializedProperty m_endPrefab;
    SerializedProperty m_middlePrefabs;    

    private void OnEnable()
    {
        m_curvySpline = serializedObject.FindProperty("m_curvySpline");
        m_populateRoot = serializedObject.FindProperty("m_populateRoot");
        m_splineSeparation = serializedObject.FindProperty("m_splineSeparation");
        m_spacing = serializedObject.FindProperty("m_spacing");
        m_groundCollisionLayer = serializedObject.FindProperty("m_groundCollisionLayer");
        m_startPrefab = serializedObject.FindProperty("m_startPrefab");
        m_endPrefab = serializedObject.FindProperty("m_endPrefab");
        m_middlePrefabs = serializedObject.FindProperty("m_middlePrefabs");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        SplinePopulator targetBhv = target as SplinePopulator;
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_curvySpline);
        EditorGUILayout.PropertyField(m_populateRoot);
        EditorGUILayout.PropertyField(m_splineSeparation);        
        EditorGUILayout.PropertyField(m_spacing);
        EditorGUILayout.PropertyField(m_groundCollisionLayer);
        if (EditorGUI.EndChangeCheck())
        {
            m_spacing.floatValue = Mathf.Max(0.1f, m_spacing.floatValue);
            serializedObject.ApplyModifiedProperties();
            targetBhv.UpdatePopulation(false);
        }

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_startPrefab);
        EditorGUILayout.PropertyField(m_endPrefab);
        EditorGUILayout.PropertyField(m_middlePrefabs);
        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
            targetBhv.UpdatePopulation(true);            
        }        
    }
}
