﻿using System;
using System.Collections;
using System.Collections.Generic;
using FluffyUnderware.Curvy;
using UnityEngine;

public class SplinePopulator : MonoBehaviour
{
    [SerializeField] CurvySpline m_curvySpline;
    [SerializeField] Transform m_populateRoot;
    [SerializeField] float m_splineSeparation = 1f;
    [SerializeField] float m_spacing = 1f;
    [SerializeField] LayerMask m_groundCollisionLayer;
    [SerializeField] GameObject[] m_startPrefab;
    [SerializeField] GameObject[] m_endPrefab;
    [SerializeField] GameObject[] m_middlePrefabs;
    [Header("Cached Instances"), Space]
    [SerializeField] List<GameObject> m_instances = new List<GameObject>();

    private int m_instanceCount;

    private void OnValidate()
    {
        if (!gameObject.scene.IsValid())
        {
            return;
        }

        if (!m_curvySpline)
        {
            m_curvySpline = GetComponentInChildren<CurvySpline>();
        }
        if (!m_populateRoot)
        {
            m_populateRoot = transform;
        }
        m_curvySpline?.OnRefresh.AddListenerOnce(HandleCurcySplineRefreshed);
    }

    private void HandleCurcySplineRefreshed(CurvySplineEventArgs arg0)
    {
        UpdatePopulation(false);
    }

    private void DestroyAllInstances()
    {
        foreach (GameObject item in m_instances)
        {
            if (item)
            {
                DestroyImmediate(item);
            }
        }
        m_instances.Clear();
    }

    GameObject GetRandom(GameObject[] objects)
    {
        var index = UnityEngine.Random.Range(0, objects.Length);
        return objects[index];
    }

    public void UpdatePopulation(bool destroyInstances)
    {
        if (!m_curvySpline)
        {
            return;
        }

        if (destroyInstances)
        {
            DestroyAllInstances();
        }
        m_instanceCount = 0;

        float splineLength = m_curvySpline.Length;
        // Instantiate Start Prefab
        if (m_startPrefab.Length > 0)
        {
            InstantiatePrefab(GetRandom(m_startPrefab), 0f);
        }

        // Instantiate End Prefab
        if (m_endPrefab.Length > 0)
        {
            InstantiatePrefab(GetRandom(m_endPrefab), splineLength);
        }

        // Instantiate Middle Prefabs
        if (m_middlePrefabs.Length > 0)
        {
            for (float dist = m_spacing; dist < splineLength; dist += m_spacing)
            {
                InstantiatePrefab(GetRandom(m_middlePrefabs), dist, m_splineSeparation);
            }
        }

        //Destroy not used instances
        for (int i = m_instanceCount; i < m_instances.Count; i++)
        {
            DestroyImmediate(m_instances[i]);
        }
        m_instances.RemoveRange(m_instanceCount, m_instances.Count - m_instanceCount);

    }

    private void InstantiatePrefab(GameObject prefab, float dist, float separation = 0f)
    {
        if (!prefab)
        {
            return;
        }

        float tf = m_curvySpline.DistanceToTF(dist);
        Vector3 tan = m_curvySpline.GetTangent(tf);
        tan.y = 0;
        Vector3 right = Vector3.Cross(tan, Vector3.up);
        Vector3 splineWorldPos = m_curvySpline.Interpolate(tf);
        splineWorldPos = m_curvySpline.ToWorldPosition(splineWorldPos);
        Vector3 prefabPos = splineWorldPos + right * separation;
        GetOrCreateInstance(prefab, prefabPos, Quaternion.LookRotation(-tan, Vector3.up));
        if (!Mathf.Approximately(separation, 0f)) // add the twin object if separation is set
        {
            prefabPos = splineWorldPos - right * separation;
            GetOrCreateInstance(prefab, prefabPos, Quaternion.LookRotation(tan, Vector3.up));
        }
    }

    private GameObject GetOrCreateInstance(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        GameObject instanceObj;
        if (m_instanceCount < m_instances.Count)
        {
            instanceObj = m_instances[m_instanceCount++];
            instanceObj.transform.position = position;
            instanceObj.transform.rotation = rotation;
        }
        else
        {
            instanceObj = Instantiate(prefab, position, rotation, m_populateRoot);
            m_instances.Add(instanceObj);
            m_instanceCount = m_instances.Count;
        }
        PlaceOverGround(instanceObj);
        return instanceObj;
    }

    private void PlaceOverGround(GameObject go)
    {
        bool saved = Physics.queriesHitTriggers;
        Physics.queriesHitTriggers = false;
        go.SetActive(false);
        if (Physics.Raycast(go.transform.position, Vector3.down, out RaycastHit hitInfo, float.MaxValue, m_groundCollisionLayer))
        {
            go.transform.position = hitInfo.point;
        }
        go.SetActive(true);
        Physics.queriesHitTriggers = saved;
    }
}
