using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Utils
{
    public class BehaviourPool<T> : MonoBehaviour where T : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] T _prefab;
        [Header("Properties")]
        [SerializeField] int _initialSize;
        [SerializeField] bool _instantiateIfNeeded = true;
        [SerializeField] bool _setPoolAsParent = true;

        public void AddToPool(T obj)
        {
            if (_setPoolAsParent)
            {
                obj.transform.parent = transform;
            }

            obj.gameObject.SetActive(false);

            _pool.Add(obj);
        }

        public T GetNext()
        {
            int num = _pool.Count;
            T obj = null;
            if (num == 0)
            {
                if (_instantiateIfNeeded)
                {
                    obj = Instantiate<T>(_prefab);
                    if (_setPoolAsParent)
                    {
                        obj.transform.parent = transform;
                    }
                }
            }
            else
            {
                obj = _pool[num - 1];
                obj.gameObject.SetActive(true);
                _pool.RemoveAt(num - 1);
            }

            return obj;
        }

        List<T> _pool;

        protected virtual void Awake()
        {
            _pool = new List<T>(_initialSize);
            for (int i = 0; i < _initialSize; i++)
            {
                var obj = Instantiate<T>(_prefab);
                if (_setPoolAsParent)
                {
                    obj.transform.parent = transform;
                }
                obj.gameObject.SetActive(false);
                _pool.Add(obj);
            }
        }
    }
}
