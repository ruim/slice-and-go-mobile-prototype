using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineUtils
{
    static public IEnumerator ExecuteWithDelay(float delay, System.Action toExecute)
    {
        yield return new WaitForSeconds(delay);
        toExecute.Invoke();
    }

    static public Coroutine ExecuteWithDelay(MonoBehaviour runner, float delay, System.Action toExecute)
    {
        return runner.StartCoroutine(ExecuteWithDelay(delay, toExecute));
    }

    static public IEnumerator ExecuteWithRealtimeDelay(float delay, System.Action toExecute)
    {
        yield return new WaitForSecondsRealtime(delay);
        toExecute.Invoke();
    }

    static public Coroutine ExecuteWithRealtimeDelay(MonoBehaviour runner, float delay, System.Action toExecute)
    {
        return runner.StartCoroutine(ExecuteWithRealtimeDelay(delay, toExecute));
    }

    static public IEnumerator ExecuteAfterFrames(int numFrames, System.Action toExecute)
    {
        for (var i = 0; i < numFrames; i++)
        {
            yield return null;
        }
        toExecute.Invoke();
    }

    static public Coroutine ExecuteAfterFrames(MonoBehaviour runner, int numFrames, System.Action toExecute)
    {
        return runner.StartCoroutine(ExecuteAfterFrames(numFrames, toExecute));
    }
}
