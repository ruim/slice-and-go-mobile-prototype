using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.Utils
{
    public class BasicSingleton<T> : MonoBehaviour where T : BasicSingleton<T>
    {
        public static T instance { get; private set; }

        public static bool exists => instance != null;

        protected virtual void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
            }
            instance = (T)this;
        }

        protected virtual void OnDestroy()
        {
            if (instance == this)
            {
                instance = null;
            }
        }

    }
}
