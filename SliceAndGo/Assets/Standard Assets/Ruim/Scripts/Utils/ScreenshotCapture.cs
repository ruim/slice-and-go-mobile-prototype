using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Ruim.Utils
{
    public class ScreenshotCapture : MonoBehaviour
    {
        [Header("Properties")]
        [SerializeField] KeyCode _saveKey = KeyCode.Space;
        [SerializeField] string _folder = "ScreenShots";
        [SerializeField] string _prefix = "Screenshot_";

        int _counter;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(_saveKey))
            {
                if (Directory.Exists(_folder) == false)
                {
                    Directory.CreateDirectory(_folder);
                }
                var file = _folder + "/" + _prefix + _counter.ToString() + ".png";
                ScreenCapture.CaptureScreenshot(file);
                Debug.Log("saved - " + file, gameObject);
                _counter++;
            }
        }

    }
}
