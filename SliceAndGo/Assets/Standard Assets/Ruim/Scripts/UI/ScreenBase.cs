using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ruim.UI
{
    [RequireComponent(typeof(Animator))]
    public abstract class ScreenBase : MonoBehaviour
    {
        [Header("Base Screen Properties")]
        [SerializeField] string _openAnim = "Open";
        [SerializeField] float _openAnimDuration = 0.5f;
        [SerializeField] string _closeAnim = "Close";
        [SerializeField] float _closeAnimDuration = 0.5f;

        Animator _animator;

        public virtual void Show(System.Action onFinished = null)
        {
            gameObject.SetActive(true);
            StartCoroutine(PlayAnimRoutine(_openAnim, _openAnimDuration, false, onFinished));
        }

        public virtual void ShowImmediate()
        {
            gameObject.SetActive(true);
        }

        public virtual void Close(System.Action onFinished = null)
        {
            StartCoroutine(PlayAnimRoutine(_closeAnim, _closeAnimDuration, true, onFinished));
        }

        public virtual void CloseImmediate()
        {
            gameObject.SetActive(false);
        }

        protected virtual void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        IEnumerator PlayAnimRoutine(string animName, float animDuration, bool disableWhenFinished, System.Action onFinished = null)
        {
            _animator.Play(animName);
            yield return new WaitForSeconds(animDuration);
            onFinished?.Invoke();
            if (disableWhenFinished)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
