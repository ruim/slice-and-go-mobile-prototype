using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ruim.UI
{
    public class LevelLabel : TextLabel
    {
        [Header("Properties")]
        [SerializeField] string _labelPrefix = "LEVEL ";

        public void SetLevel(int level)
        {
            var text = _labelPrefix + level.ToString();
            SetText(text);
        }
    }
}