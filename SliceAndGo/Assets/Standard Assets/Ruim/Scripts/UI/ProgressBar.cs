using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ruim.UI
{
    public class ProgressBar : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] Image _fillImage;
        [SerializeField] Image _backImage;
        [SerializeField] Image _outlineImage;

        [Header("Properties")]
        [SerializeField] float _fillSpeed = 2f;

        float _targetProgress;

        public virtual void SetProgress(float progress, bool animate = true)
        {
            _targetProgress = progress;
            if (animate == false)
            {
                _fillImage.fillAmount = progress;
            }
        }

        public void SetBackColor(Color color) => _backImage.color = color;

        public void SetOutlineColor(Color color) => _outlineImage.color = color;

        protected virtual void Update()
        {
            float currFill = _fillImage.fillAmount;
            float maxDelta = _fillSpeed * Time.deltaTime;
            _fillImage.fillAmount = Mathf.MoveTowards(currFill, _targetProgress, maxDelta);
        }
    }
}
