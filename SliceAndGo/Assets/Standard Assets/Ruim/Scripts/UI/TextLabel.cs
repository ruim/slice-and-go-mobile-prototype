using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ruim.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class TextLabel : MonoBehaviour
    {

        public string text => _labels[0].text;

        public void SetText(string text)
        {
            foreach (var label in _labels)
            {
                label.text = text;
            }
        }

        Text[] _labels;

        void Awake()
        {
            _labels = GetComponentsInChildren<Text>();
        }
    }
}