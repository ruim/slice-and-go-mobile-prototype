using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Ruim.UI
{
    public class ScreenController : MonoBehaviour
    {
        [Header("Properties")]
        [SerializeField] bool _hideAllOnStart;

        List<ScreenBase> _screens = new List<ScreenBase>();
        Dictionary<Type, ScreenBase> _typeToScreen = new Dictionary<Type, ScreenBase>();

        bool _firstTime;

        public ScreenBase ActiveScreen { get; private set; }

        public void ShowScreen<T>(float delay = 0f, System.Action onFinished = null) where T : ScreenBase
        {
            if (delay > 0f)
            {
                StartCoroutine(ShowScreenWithDelay<T>(delay, onFinished));
            }
            else
            {
                ShowScreenImpl<T>(onFinished);
            }
        }

        public T GetScreen<T>() where T : ScreenBase
        {
            return _typeToScreen[typeof(T)] as T;
        }

        private void Awake()
        {
            GetComponentsInChildren(true, _screens);
            foreach (ScreenBase screen in _screens)
            {
                var type = screen.GetType();
                _typeToScreen.Add(type, screen);
            }
        }

        void Start()
        {
            if (_hideAllOnStart)
            {
                foreach (var screen in _screens)
                {
                    screen.CloseImmediate();
                }
            }
        }

        IEnumerator ShowScreenWithDelay<T>(float delay, System.Action onFinished) where T : ScreenBase
        {
            yield return new WaitForSeconds(delay);
            ShowScreenImpl<T>(onFinished);
        }

        void ShowScreenImpl<T>(System.Action onFinished) where T : ScreenBase
        {
            if (ActiveScreen != null)
            {
                ActiveScreen.Close();
                ActiveScreen = _typeToScreen[typeof(T)];
                ActiveScreen.Show(onFinished);
            }
            else
            {
                //it's the first time, so close all screens before showing the correct one
                foreach (ScreenBase screen in _screens)
                {
                    screen.gameObject.SetActive(false);
                }
                ActiveScreen = _typeToScreen[typeof(T)];
                ActiveScreen.ShowImmediate();
                onFinished?.Invoke();
            }
        }

    }
}
