using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScenesList : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] string[] _levels;

    [Header("DEBUG")]
    [SerializeField] string _debugSceneName;
    [SerializeField] bool _useDebugScene;

    public string GetLevelSceneName(int levelNumber)
    {
#if UNITY_EDITOR
        if (_useDebugScene)
        {
            return _debugSceneName;
        }
#endif
        var index = levelNumber - FIRST_LEVEL_VALUE;
        var wrappedIndex = index % _levels.Length;

        return _levels[wrappedIndex].ToString();
    }

    const int FIRST_LEVEL_VALUE = 1;
}
