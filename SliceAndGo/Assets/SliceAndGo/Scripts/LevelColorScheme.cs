using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelColorScheme", menuName = "SliceAndGo/LevelColorScheme", order = 0)]
public class LevelColorScheme : ScriptableObject
{
    [Header("Properties")]
    [SerializeField] Color _sliceableShapes = Color.white;
    [SerializeField] Color _staticShapes = Color.white;
    [SerializeField] Color _back = Color.white;

    [Header("Control")]
    [SerializeField] LevelColorScheme _copyFrom;

    public System.Action onChanged;

    public Color sliceableShapes => _sliceableShapes;
    public Color staticShapes => _staticShapes;
    public Color back => _back;

    public void SetupFromOther(LevelColorScheme other)
    {
        _back = other.back;
        _sliceableShapes = other.sliceableShapes;
        _staticShapes = other.staticShapes;
    }

    private void OnValidate()
    {
        if (_copyFrom != null)
        {
            SetupFromOther(_copyFrom);
            _copyFrom = null;
        }
        onChanged?.Invoke();
    }
}


