using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Cinemachine;

#if UNITY_EDITOR
public class RecordSessionOverrides : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] Vector3 _positionOffset;
    [SerializeField] float _zoomIn;

    class CamDefaults
    {

        public CinemachineVirtualCamera vcam;
        public Vector3 defaultPosition;
        public Vector3 defaultForward;

        public CamDefaults(CinemachineVirtualCamera vcam)
        {
            this.vcam = vcam;
            defaultPosition = vcam.transform.position;
            defaultForward = vcam.transform.forward;
        }
    }

    List<CinemachineVirtualCamera> _vcams = new List<CinemachineVirtualCamera>();
    Dictionary<CinemachineVirtualCamera, CamDefaults> _vcamToDefaults = new Dictionary<CinemachineVirtualCamera, CamDefaults>();

    private IEnumerator Start()
    {
        var levelRoot = gameObject.scene.GetRootGameObjects()[0];
        levelRoot.GetComponentsInChildren(_vcams);
        foreach (var vcam in _vcams)
        {
            var defaults = new CamDefaults(vcam);
            _vcamToDefaults.Add(vcam, defaults);
        }
        yield return null;
        ApplyOverrides();
    }


    private void OnValidate()
    {
        if (Application.isPlaying == false)
        {
            return;
        }
        ApplyOverrides();
    }

    void ApplyOverrides()
    {
        foreach (var vcam in _vcams)
        {
            var defaults = _vcamToDefaults[vcam];
            var pos = defaults.defaultPosition + _positionOffset;
            pos += defaults.defaultForward * _zoomIn;

            vcam.transform.position = pos;
        }
    }
}
#endif
