using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelRoot : MonoBehaviour
{
    [Header("References")]
    [SerializeField] LevelController _levelController;
    [SerializeField] LevelFlowController _levelCameraController;
    [SerializeField] GameObject _confettiVFX;
    [SerializeField] BallsController _ballsController;
    [SerializeField] LevelFlowController _flowController;

    public LevelController levelController => _levelController;
    public LevelFlowController cameraController => _levelCameraController;
    public GameObject confettiVFX => _confettiVFX;
    public BallsController ballsController => _ballsController;
    public LevelFlowController flowController => _flowController;
}
