using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class LevelSectionTrigger : MonoBehaviour
{

    public System.Action onBallsEntered;
    public System.Action onBallsExited;

    public int numBalls { get; private set; }

    void Awake()
    {
        GetComponent<Collider2D>().isTrigger = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Ball ball))
        {
            numBalls++;
            if (numBalls == 1)
            {
                onBallsEntered?.Invoke();
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out Ball ball))
        {
            numBalls--;
            if (numBalls == 0)
            {
                onBallsExited?.Invoke();
            }
        }
    }
}
