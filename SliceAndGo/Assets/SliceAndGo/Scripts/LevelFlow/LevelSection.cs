using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Ruim.Math;
using UnityEngine;
using UnityEngine.Serialization;

public class LevelSection : MonoBehaviour
{

    [Header("References")]
    [SerializeField] GameObject _sliceableShapesParent;
    [SerializeField] GameObject _gatesParent;

    [Header("Properties")]
    [SerializeField] RangeF _removeShapesDelay;
    [SerializeField] bool _removeShapesFromLastSectionWhenActivated = true;

    [FormerlySerializedAs("_pausesCameraWhenSpawningBalls")]
    [SerializeField] bool _pausesTransitionWhenSpawningBalls = true;


    public System.Action<LevelSection> onBallsEnteredTrigger;

    public System.Action<LevelSection> onBallsExitedTrigger;

    public System.Action<LevelSection> onTransitionShouldPause;

    public bool hasBallsInTrigger => _cameraTrigger.numBalls > 0;

    public int GetNumBallsInTrigger() => _cameraTrigger.numBalls;

    public bool removeShapesFromLastSectionWhenActivated => _removeShapesFromLastSectionWhenActivated;


    public void DisableTrigger()
    {
        _cameraTrigger.onBallsEntered -= OnBallsEntered;
        _cameraTrigger.onBallsExited -= OnBallsExited;
        _cameraTrigger.gameObject.SetActive(false);
    }

    public void SetActiveSection(bool active)
    {
        _virtualCamera.SetActiveCamera(active);
    }

    public void RemoveReleasedShapes()
    {
        for (var i = _shapes.Count - 1; i >= 0; i--)
        {
            var shape = _shapes[i];
            if (shape.hasBeenReleased)
            {
                //don't respond to this event anymore because we are already removing from _shapes list
                shape.onShapeWillRemove -= OnShapeWillRemove;
                shape.RemoveWithDelay(_removeShapesDelay.random);
                _shapes.RemoveAt(i);
            }
        }
    }



    LevelSectionTrigger _cameraTrigger;
    LevelSectionVirtualCamera _virtualCamera;
    List<SliceableShape> _shapes = new List<SliceableShape>();
    List<GateBase> _gates = new List<GateBase>();

    void Awake()
    {
        _cameraTrigger = GetComponentInChildren<LevelSectionTrigger>();
        _cameraTrigger.onBallsEntered += OnBallsEntered;
        _cameraTrigger.onBallsExited += OnBallsExited;
        _virtualCamera = GetComponentInChildren<LevelSectionVirtualCamera>();

        _sliceableShapesParent.GetComponentsInChildren(_shapes);
        _gatesParent.GetComponentsInChildren(_gates);
    }

    private void Start()
    {
        foreach (var shape in _shapes)
        {
            shape.onSliced += OnShapeSliced;
            shape.onShapeWillRemove += OnShapeWillRemove;
        }
        GateEvents.onShouldSpawnBall += OnGateShouldSpawnBall;
    }

    void OnDestroy()
    {
        GateEvents.onShouldSpawnBall -= OnGateShouldSpawnBall;
    }

    void OnBallsEntered() => onBallsEnteredTrigger?.Invoke(this);
    void OnBallsExited() => onBallsExitedTrigger?.Invoke(this);

    void OnShapeSliced(SliceableShape original, SliceableShape positiveSlice, SliceableShape negativeSlice)
    {
        _shapes.Remove(original);
        _shapes.Add(positiveSlice);
        _shapes.Add(negativeSlice);
        positiveSlice.onSliced += OnShapeSliced;
        negativeSlice.onSliced += OnShapeSliced;

        if (original.IsCountingDownToRemove())
        {
            positiveSlice.RemoveWithDelay(original.removeCountdown);
            negativeSlice.RemoveWithDelay(original.removeCountdown);
        }
    }

    void OnGateShouldSpawnBall(SpawnerGateBase sender, Vector3 pos)
    {
        if (_pausesTransitionWhenSpawningBalls && _gates.Contains(sender))
        {
            onTransitionShouldPause?.Invoke(this);
        }
    }

    void OnShapeWillRemove(SliceableShape shape)
    {
        _shapes.Remove(shape);
    }

}
