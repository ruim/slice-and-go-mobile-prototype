using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Cinemachine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class LevelSectionVirtualCamera : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] float _screenShakeIntensity = 1f;
    [SerializeField] float _screenShakeDuration = 0.5f;

    [Header("DEBUG")]
    [SerializeField] bool _debugScreenShake;

    public void SetActiveCamera(bool active)
    {
        _vcam.Priority = active ? ACTIVE_CAMERA_PRIORITY : INACTIVE_CAMERA_PRIORITY;
        _active = active;
    }

    const int ACTIVE_CAMERA_PRIORITY = 100;
    const int INACTIVE_CAMERA_PRIORITY = 0;

    CinemachineVirtualCamera _vcam;
    bool _active;
    Coroutine _shakeRoutine;

    private void Awake()
    {
        _vcam = GetComponent<CinemachineVirtualCamera>();
    }

    private void Start()
    {
        BombEvents.onBombExploded += OnBombExploded;
    }

    private void OnDestroy()
    {
        BombEvents.onBombExploded -= OnBombExploded;
    }

    void ApplyScreenShake()
    {
        if (_shakeRoutine != null)
        {
            StopCoroutine(_shakeRoutine);
        }
        _shakeRoutine = StartCoroutine(ShakeRoutine());
    }

    void OnBombExploded(Bomb bomb)
    {
        ApplyScreenShake();
    }

    IEnumerator ShakeRoutine()
    {
        var noise = _vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        var startAmp = _screenShakeIntensity;
        var endAmp = 0f;

        noise.m_AmplitudeGain = startAmp;
        var progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / _screenShakeDuration;
            noise.m_AmplitudeGain = Mathf.Lerp(startAmp, endAmp, progress);
            yield return null;
        }
        noise.m_AmplitudeGain = 0f;
    }

    private void OnValidate()
    {
        if (_vcam == null)
        {
            return;
        }
        if (_debugScreenShake)
        {
            _debugScreenShake = false;
            ApplyScreenShake();
        }
    }
}
