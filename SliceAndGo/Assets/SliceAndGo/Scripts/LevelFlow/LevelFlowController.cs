using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class LevelFlowController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] GameObject _levelSectionsParent;

    [SerializeField] BallsController _ballsController;

    [Header("Properties")]
    [SerializeField] float _transitionPauseDuration = 1f;
    [SerializeField] int _minNumBallsToGoBack = 20;


    public Camera cam { get; private set; }

    public LevelSection lastSection => _levelSections[_levelSections.Count - 1];

    public System.Action onLevelReachedDeadEnd;

    const float CINEMACHINE_DELTA_TIME_PAUSED_VALUE = 0f;
    const float CINEMACHINE_DELTA_TIME_DONT_USE_OVERRIDE = -1f; //any negative value

    List<LevelSection> _levelSections = new List<LevelSection>();
    LevelSection _activeSection;
    bool _levelComplete;
    float _transitionPausedCountdown;
    LevelSection _sectionToActivateAfterPause;

    void Awake()
    {
        cam = GetComponentInChildren<Camera>();
        _levelSectionsParent.GetComponentsInChildren(_levelSections);
    }

    void Start()
    {
        ActivateSection(_levelSections[0]);
        _levelSections[0].DisableTrigger();
        if (_levelSections.Count > 1)
        {
            for (var i = 1; i < _levelSections.Count; i++)
            {
                var levelSection = _levelSections[i];
                if (levelSection != _activeSection)
                {
                    levelSection.SetActiveSection(false);
                }
                levelSection.onBallsEnteredTrigger += OnBallsEnteredSectionTrigger;
            }
        }

        _ballsController.onComplete += () => _levelComplete = true;

        foreach (var section in _levelSections)
        {
            section.onTransitionShouldPause += OnLevelSectionTransitionShouldPause;
        }
    }

    void Update()
    {
        if (_transitionPausedCountdown > 0f)
        {
            _transitionPausedCountdown -= Time.deltaTime;
            if (_transitionPausedCountdown < Mathf.Epsilon)
            {
                _transitionPausedCountdown = 0f;
                CinemachineCore.UniformDeltaTimeOverride = CINEMACHINE_DELTA_TIME_DONT_USE_OVERRIDE;

                if (_sectionToActivateAfterPause != null)
                {
                    ActivateSection(_sectionToActivateAfterPause);
                    _sectionToActivateAfterPause = null;
                }
            }
        }
    }

    void OnBallsEnteredSectionTrigger(LevelSection levelSection)
    {
        if (_activeSection == null)
        {
            ActivateSection(levelSection);
            return;
        }

        var newSectionIndex = GetLevelSectionIndex(levelSection);
        var currSectionIndex = GetLevelSectionIndex(_activeSection);
        if (newSectionIndex > currSectionIndex)
        {
            if (levelSection.removeShapesFromLastSectionWhenActivated)
            {
                _activeSection.RemoveReleasedShapes();
            }
            if (_transitionPausedCountdown > 0f)
            {
                _sectionToActivateAfterPause = levelSection;
            }
            else
            {
                ActivateSection(levelSection);
            }
        }
    }

    void OnLevelSectionTransitionShouldPause(LevelSection levelSection)
    {
        CinemachineCore.UniformDeltaTimeOverride = CINEMACHINE_DELTA_TIME_PAUSED_VALUE;
        _transitionPausedCountdown = _transitionPauseDuration;
    }

    int GetLevelSectionIndex(LevelSection levelSection)
    {
        for (var i = 0; i < _levelSections.Count; i++)
        {
            if (levelSection == _levelSections[i])
            {
                return i;
            }
        }
        return -1;
    }

    void ActivateSection(LevelSection levelSection)
    {
        if (_activeSection == levelSection)
        {
            return;
        }
        if (_activeSection != null)
        {
            _activeSection.SetActiveSection(false);
        }
        _activeSection = levelSection;
        _activeSection.SetActiveSection(true);
    }


}
