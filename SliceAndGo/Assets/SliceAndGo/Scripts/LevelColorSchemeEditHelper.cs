using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
public class LevelColorSchemeEditHelper : MonoBehaviour
{
    [Header("Color Scheme")]
    [SerializeField] LevelColorScheme _levelColorScheme;

    [Header("Control")]
    [SerializeField] bool _createLevelMaterials;
    [SerializeField] bool _applyMaterials;

    [SerializeField] bool _applyColorScheme;

    [Header("References")]
    [SerializeField] Material _baseSliceableShapesMaterial;
    [SerializeField] Material _baseStaticShapesMaterial;
    [SerializeField] Material _baseBackMaterial;

    [Header("DEBUG")]
    [SerializeField] Material _sliceableShapesMaterial;
    [SerializeField] Material _staticShapesMaterial;
    [SerializeField] Material _backMaterial;

    public void CreateLevelMaterials()
    {
        DestroyPrevMaterialAssetsIfExist();
        var folder = CreateOrGetExistingFolder();

        if (_sliceableShapesMaterial == null)
        {
            _sliceableShapesMaterial = Instantiate(_baseSliceableShapesMaterial);
        }

        var assetPrefix = gameObject.scene.name + "_";
        var assetPath = folder + "/" + assetPrefix + SLICEABLE_SHAPES_MATERIAL_ASSET_BASE_NAME;
        AssetDatabase.CreateAsset(_sliceableShapesMaterial, assetPath);

        if (_staticShapesMaterial == null)
        {
            _staticShapesMaterial = Instantiate(_baseStaticShapesMaterial);
        }
        assetPath = folder + "/" + assetPrefix + STATIC_SHAPES_MATERIAL_ASSET_BASE_NAME;
        AssetDatabase.CreateAsset(_staticShapesMaterial, assetPath);

        if (_backMaterial == null)
        {
            _backMaterial = Instantiate(_baseBackMaterial);
        }
        assetPath = folder + "/" + assetPrefix + BACK_MATERIAL_ASSET_BASE_NAME;
        AssetDatabase.CreateAsset(_backMaterial, assetPath);
    }

    public void ApplyMaterials()
    {
        var root = gameObject.scene.GetRootGameObjects()[0];
        if (_backMaterial == null || _sliceableShapesMaterial == null || _staticShapesMaterial == null)
        {
            Debug.Log("Cannot apply materials because 1 or more material is NULL!, Create material assets first");
            return;
        }
        var backPlane = root.GetComponentInChildren<BackgroundPlane>();
        backPlane.GetComponent<MeshRenderer>().sharedMaterial = _backMaterial;

        var sliceableShapes = root.GetComponentsInChildren<SliceableShape>();
        ApplyMaterialToShapes(_sliceableShapesMaterial, sliceableShapes);

        var staticShapes = root.GetComponentsInChildren<StaticShape>();
        ApplyMaterialToShapes(_staticShapesMaterial, staticShapes);
    }

    public void ApplyColorScheme()
    {
        if (_levelColorScheme == null)
        {
            Debug.Log("Level Color Scheme is null!");
            return;
        }
        if (_sliceableShapesMaterial == null || _staticShapesMaterial == null || _backMaterial == null)
        {
            Debug.Log("Cannot apply color scheme because 1 or more materials is null... create materials first");
        }
        _sliceableShapesMaterial.color = _levelColorScheme.sliceableShapes;
        _staticShapesMaterial.color = _levelColorScheme.staticShapes;
        _backMaterial.color = _levelColorScheme.back;
    }

    const string PARENT_FOLDER = "Assets/SliceAndGo/Materials";

    const string SLICEABLE_SHAPES_MATERIAL_ASSET_BASE_NAME = "SliceableShapes.mat";
    const string STATIC_SHAPES_MATERIAL_ASSET_BASE_NAME = "StaticShapes.mat";
    const string BACK_MATERIAL_ASSET_BASE_NAME = "Back.mat";

    LevelColorScheme _currColorScheme;

    void Start()
    {
        _currColorScheme = _levelColorScheme;
    }

    private void Update()
    {
        if (_createLevelMaterials)
        {
            _createLevelMaterials = false;
            CreateLevelMaterials();
        }
        if (_applyMaterials)
        {
            _applyMaterials = false;
            ApplyMaterials();
        }
        if (_applyColorScheme)
        {
            _applyColorScheme = false;
            ApplyColorScheme();
        }
    }

    string GetFolderName() => gameObject.scene.name;

    string CreateOrGetExistingFolder()
    {
        var folderName = GetFolderName();
        var folderPath = PARENT_FOLDER + "/" + folderName;
        var folderExists = AssetDatabase.IsValidFolder(folderPath);
        if (folderExists == false)
        {
            AssetDatabase.CreateFolder(PARENT_FOLDER, folderName);
        }
        return folderPath;
    }

    void DestroyPrevMaterialAssetsIfExist()
    {
        void DestroyMaterialIfExists(ref Material matRef)
        {
            if (AssetDatabase.Contains(matRef))
            {
                DestroyImmediate(matRef, true);
                matRef = null;
            }
        }
        DestroyMaterialIfExists(ref _sliceableShapesMaterial);
        DestroyMaterialIfExists(ref _staticShapesMaterial);
        DestroyMaterialIfExists(ref _backMaterial);
    }

    void ApplyMaterialToShapes<T>(Material mat, T[] shapes) where T : ShapeBase
    {
        foreach (var shape in shapes)
        {
            var shapeRend = shape.GetComponentInChildren<ShapeRenderer>();
            shapeRend.ApplySharedMaterial(mat);
        }
    }

    private void OnValidate()
    {
        if (_levelColorScheme != _currColorScheme)
        {
            if (_currColorScheme != null)
            {
                _currColorScheme.onChanged -= OnColorSchemeChanged;
            }
            _currColorScheme = _levelColorScheme;
            _currColorScheme.onChanged += OnColorSchemeChanged;
            ApplyColorScheme();
        }
    }

    void OnColorSchemeChanged()
    {
        ApplyColorScheme();
    }
}
#endif