using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class InLevelText : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] float _life = 1f;
    [SerializeField] float _upSpeed = 1f;
    [SerializeField] float _removeDuration = 0.5f;
    [SerializeField] AnimationCurve _scaleMultOverLife;

    [Header("References")]
    [SerializeField] TextMeshPro _text;

    public void Setup(Vector3 position, string text, Color color, System.Action<InLevelText> onFinished = null)
    {
        transform.position = position;
        transform.localScale = _defaultScale;
        _text.text = text;
        _text.color = color;
        _progress = 0f;
        _onFinished = onFinished;
        ApplyScale();
        gameObject.SetActive(true);
    }

    Vector3 _defaultScale;
    float _progress;
    System.Action<InLevelText> _onFinished;

    private void Awake()
    {
        _defaultScale = transform.localScale;
    }

    private void Update()
    {
        if (_progress > 1f)
        {
            return;
        }
        transform.position += Vector3.up * (Time.deltaTime * _upSpeed);

        _progress += Time.deltaTime / _life;
        ApplyScale();
        if (_progress > 1f)
        {
            _onFinished?.Invoke(this);
        }
    }

    void ApplyScale()
    {
        var sizeMult = _scaleMultOverLife.Evaluate(_progress);
        transform.localScale = _defaultScale * sizeMult;
    }
}
