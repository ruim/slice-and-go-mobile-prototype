using System.Collections;
using System.Collections.Generic;
using HomaGames.HomaBelly;
using UnityEngine;

public class SliceFTUE : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] float _speed;
    [SerializeField] float _clickScaleMult = 0.8f;

    [Header("References")]
    [SerializeField] Transform _start;
    [SerializeField] Transform _end;
    [SerializeField] Transform _icon;

    [Header("DEBUG")]
    [SerializeField] bool _debugPlay;

    public void PlayIfFirstTime()
    {
        if (_firstPlay)
        {
            _firstPlay = false;
            SaveFTUEComplete(false);
        }
        if (IsFTUEComplete() == false)
        {
            gameObject.SetActive(true);
            StartCoroutine(AnimationRoutine());
            _playing = true;
            DefaultAnalytics.TutorialStepStarted("SliceTutorial");
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    static bool _firstPlay = true;
    const string COMPLETED_FTUE_KEY = "completed_ftue";
    bool _playing;

    const int TRUE_VALUE = 1;
    const int FALSE_VALUE = 0;


    Vector3 _defaultIconScale;


    private void Awake()
    {
        _defaultIconScale = _icon.localScale;
        SliceableShape.onFirstShapeSliced += OnFirstShapeSliced;
    }

    private void OnDestroy()
    {
        SliceableShape.onFirstShapeSliced -= OnFirstShapeSliced;
    }

    IEnumerator AnimationRoutine()
    {
        while (true)
        {
            _icon.position = _start.position;
            _icon.localScale = _defaultIconScale;
            yield return new WaitForSeconds(0.25f);
            _icon.localScale = _defaultIconScale * _clickScaleMult;
            var dist = Vector2.Distance(_start.position, _end.position);
            var duration = dist / _speed;
            var progress = 0f;
            while (progress < 1f)
            {
                progress += Time.deltaTime / duration;
                _icon.position = Vector3.Lerp(_start.position, _end.position, progress);
                yield return null;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    void SaveFTUEComplete(bool complete)
    {
        var value = complete ? TRUE_VALUE : FALSE_VALUE;
        PlayerPrefs.SetInt(COMPLETED_FTUE_KEY, value);
        PlayerPrefs.Save();
    }

    bool IsFTUEComplete()
    {
        return PlayerPrefs.GetInt(COMPLETED_FTUE_KEY, FALSE_VALUE) == TRUE_VALUE;
    }

    void OnFirstShapeSliced(SliceableShape sender)
    {
        if (_playing)
        {
            _playing = false;
            StopAllCoroutines();
            SaveFTUEComplete(true);
            gameObject.SetActive(false);
            DefaultAnalytics.TutorialStepCompleted();
        }
    }

    private void OnValidate()
    {
        if (_debugPlay)
        {
            _debugPlay = false;
            if (Application.isPlaying)
            {
                StartCoroutine(AnimationRoutine());
                _playing = true;
            }
        }
    }

}
