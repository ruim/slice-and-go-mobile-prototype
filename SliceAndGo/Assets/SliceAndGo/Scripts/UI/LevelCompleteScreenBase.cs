using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ruim.UI;

public abstract class LevelCompleteScreenBase : ScreenBase
{
    public void Setup(int levelNumber)
    {
        var levelLabel = GetComponentInChildren<LevelLabel>();
        levelLabel.SetLevel(levelNumber);
    }
}
