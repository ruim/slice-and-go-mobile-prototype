using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ruim.UI;

public class LevelScreen : ScreenBase
{
    [Header("References")]
    [SerializeField] Text _numBallsText;
    [SerializeField] Button _restartButton;
    [SerializeField] SliceFTUE _sliceFTUE;

    [Header("DEBUG")]
    //if it is a recording session don't show FTUE nor Restart button
    [SerializeField] bool _debugIsRecordingSession;


    public event System.Action onRestartButtonPressed;

    public void Setup(int levelNumber, BallsController ballsController)
    {
        var levelLabel = GetComponentInChildren<LevelLabel>();
        levelLabel.SetLevel(levelNumber);
        _ballsController = ballsController;
    }

    public override void Show(System.Action onFinished)
    {
        base.Show(onFinished);
        TryShowFTUE();
#if UNITY_EDITOR
        if (_debugIsRecordingSession)
        {
            _restartButton.gameObject.SetActive(false);
        }
#endif
    }

    public override void ShowImmediate()
    {
        base.ShowImmediate();
        TryShowFTUE();
#if UNITY_EDITOR
        if (_debugIsRecordingSession)
        {
            _restartButton.gameObject.SetActive(false);
        }
#endif
    }

    BallsController _ballsController;

    protected override void Awake()
    {
        base.Awake();
        BallEvents.onBallCreated += OnBallCreated;
        BallEvents.onBallDestroyed += OnBallDestroyed;
        _numBallsText.text = "0";
        _restartButton.onClick.AddListener(OnRestartButtonPressed);
    }

    private void OnDestroy()
    {
        BallEvents.onBallCreated -= OnBallCreated;
        BallEvents.onBallDestroyed -= OnBallDestroyed;
    }

    void UpdateText() => _numBallsText.text = _ballsController.numBalls.ToString();

    void TryShowFTUE()
    {
#if UNITY_EDITOR
        if (_debugIsRecordingSession)
        {
            _sliceFTUE.gameObject.SetActive(false);
        }
        else
        {
            _sliceFTUE.PlayIfFirstTime();
        }
#else
        _sliceFTUE.PlayIfFirstTime();
#endif
    }

    void OnBallCreated(Ball ball)
    {
        UpdateText();
    }

    void OnBallDestroyed(Ball ball)
    {
        UpdateText();
    }

    void OnRestartButtonPressed()
    {
        onRestartButtonPressed?.Invoke();
    }
}
