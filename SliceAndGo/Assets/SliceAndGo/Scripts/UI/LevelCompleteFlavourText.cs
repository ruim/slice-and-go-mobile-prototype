using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompleteFlavourText : MonoBehaviour
{
    [SerializeField] string[] _possibleTexts;

    [SerializeField] Text text;

    void Start()
    {
        var str = _possibleTexts[Random.Range(0, _possibleTexts.Length)];
        text.text = str;
    }
}
