using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ruim.UI;

public class BallsInCupProgressBar : ProgressBar
{
    [SerializeField] Text _counter;
    [SerializeField] BallsController _ballsController;

    private void Awake()
    {
        _ballsController.onProgressChanged += OnProgressChanged;
        _ballsController.onBallsFinishedSpawning += OnBallsFinishedSpawning;
    }

    void Start()
    {
        SetProgress(0f, animate: false);
    }

    void OnProgressChanged()
    {
        var progress = _ballsController.GetProgress01();
        SetProgress(progress);
        UpdateText();
    }

    void UpdateText()
    {
        var numNeeded = _ballsController.numBallsToWin;
        var numInCup = _ballsController.numBallsInCup;
        _counter.text = numInCup.ToString() + "/" + numNeeded.ToString();
    }

    void OnBallsFinishedSpawning()
    {
        UpdateText();
    }
}
