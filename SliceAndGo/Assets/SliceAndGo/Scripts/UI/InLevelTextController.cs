using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(InLevelTextPool))]
public class InLevelTextController : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] float _textDepthOffset = -1f;
    [SerializeField] float _minIntervalBetweenTexts = 0.1f;
    [SerializeField] string _spawnBallText = "+1";
    [SerializeField] string _removeBallText = "-1";
    [SerializeField] string _ballEnterCupText = "+1";

    [SerializeField] Color _goodColor = Color.white;
    [SerializeField] Color _badColor = Color.red;

    [Header("References")]
    [SerializeField] BallCup _ballCup;

    InLevelTextPool _pool;
    float _nextSpawnCountdown;

    private void Awake()
    {
        _pool = GetComponent<InLevelTextPool>();
    }

    private void Start()
    {
        GateEvents.onShouldSpawnBall += OnGateShouldSpawnBall;
        GateEvents.onBallWillBeRemoved += OnGateWillRemoveBall;
        _ballCup.onBallEnter += OnBallEnterCup;
    }

    private void OnDestroy()
    {
        GateEvents.onShouldSpawnBall -= OnGateShouldSpawnBall;
        GateEvents.onBallWillBeRemoved -= OnGateWillRemoveBall;
        _ballCup.onBallEnter -= OnBallEnterCup;
    }

    private void Update()
    {
        if (_nextSpawnCountdown > 0f)
        {
            _nextSpawnCountdown -= Time.deltaTime;
        }
    }

    void Show(Vector3 pos, string text, bool isGood)
    {
        var inLevelText = _pool.GetNext();
        var color = isGood ? _goodColor : _badColor;
        inLevelText.Setup(pos, text, color, OnTextFinished);
        _nextSpawnCountdown = _minIntervalBetweenTexts;
    }

    void OnTextFinished(InLevelText inLevelText)
    {
        _pool.AddToPool(inLevelText);
    }

    void OnGateShouldSpawnBall(SpawnerGateBase sender, Vector3 ballPos)
    {
        if (_nextSpawnCountdown > 0f)
        {
            return;
        }
        var textPos = GetTextPosForGate(sender, ballPos.x);
        Show(textPos, _spawnBallText, true);
    }

    void OnGateWillRemoveBall(RemoverGateBase sender, Ball ball)
    {
        if (_nextSpawnCountdown > 0f)
        {
            return;
        }
        var textPos = GetTextPosForGate(sender, ball.transform.position.x);
        Show(textPos, _removeBallText, false);
    }

    void OnBallEnterCup(Ball ball)
    {
        if (_nextSpawnCountdown > 0f)
        {
            return;
        }
        var triggerStart = _ballCup.GetTriggerStart() + Vector3.left;
        var triggerEnd = _ballCup.GetTriggerEnd() + Vector3.right;
        var textPos = Vector3.Lerp(triggerStart, triggerEnd, Random.value);
        textPos.y += Random.Range(1f, 2f);
        textPos.z = _textDepthOffset;
        Show(textPos, _ballEnterCupText, true);
    }

    Vector3 GetTextPosForGate(GateBase gate, float x)
    {
        var textPos = new Vector3(x, 0f, _textDepthOffset);
        textPos.y = gate.transform.position.y + Random.Range(1f, 2f);
        return textPos;
    }



}
