using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompleteEmoticon : MonoBehaviour
{
    [SerializeField] Sprite[] _possibleSprites;

    void Start()
    {
        var sprite = _possibleSprites[Random.Range(0, _possibleSprites.Length)];

        var images = GetComponentsInChildren<Image>();
        foreach (var img in images)
        {
            img.sprite = sprite;
        }
    }

}
