using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdditionGate : SpawnerGateBase
{

    [SerializeField] int _add;

    [SerializeField] float _intervalPerBall = 0.1f;

    protected override void DoBallEnterAction(Ball ball)
    {
        if (_alreadyUsed)
        {
            return;
        }
        _alreadyUsed = true;
        StartCoroutine(SpawnRoutine());
    }

    protected override string GetText()
    {
        return "+" + _add.ToString();
    }

    bool _alreadyUsed;

    IEnumerator SpawnRoutine()
    {
        var text = GetComponentInChildren<Text>();
        var wait = new WaitForSeconds(_intervalPerBall);
        while (_add > 0)
        {
            SpawnBalls(1);
            _add--;
            text.text = GetText();
            yield return wait;
        }
        DeactivateGate();
    }


}
