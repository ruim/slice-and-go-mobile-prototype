using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(GateActionBodyAnimation))]
public abstract class GateBase : MonoBehaviour
{

    public System.Action<GateBase> onGateEnteredFirstTime;

    public System.Action<GateBase> onGateWillBeDeactivated;

    protected abstract void DoBallEnterAction(Ball ball);

    protected abstract string GetText();

    protected BoxCollider2D boxCol { get; private set; }

    protected void DeactivateGate()
    {
        _active = false;
        onGateWillBeDeactivated?.Invoke(this);
        gameObject.SetActive(false);
    }

    const float ENTER_BALL_SCALE_MULT = 1.3f;

    GateActionBodyAnimation _actionBodyAnimation;
    bool _entered;
    bool _active;


    protected virtual void Awake()
    {
        _actionBodyAnimation = GetComponent<GateActionBodyAnimation>();
        boxCol = GetComponent<BoxCollider2D>();
        boxCol.isTrigger = true;
        _active = true;
    }


    Vector3 GetSpawnPos()
    {
        var size = boxCol.size;
        var x = Random.Range(-0.5f, 0.5f) * size.x;
        var bottom = -size.y * 0.5f;
        var y = Random.Range(bottom - 1f, bottom);
        return transform.localToWorldMatrix.MultiplyPoint3x4(new Vector3(x, y, 0f));
    }

    void ApplyText()
    {
        var text = GetComponentInChildren<Text>();
        text.text = GetText();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (_active == false)
        {
            return;
        }
        if (other.TryGetComponent(out Ball ball))
        {
            if (_entered == false)
            {
                _entered = true;
                onGateEnteredFirstTime?.Invoke(this);
            }
            if (ball.spawner == gameObject)
            {
                return;
            }
            _actionBodyAnimation.Play();
            DoBallEnterAction(ball);
        }
    }

    private void OnValidate()
    {
        ApplyText();
    }

}