using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RemoverGateBase : GateBase
{
    protected void RemoveBall(Ball ball)
    {
        GateEvents.SendBallWillBeRemoved(this, ball);
        Destroy(ball);
    }
}
