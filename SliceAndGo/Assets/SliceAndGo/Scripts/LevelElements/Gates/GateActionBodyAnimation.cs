using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateActionBodyAnimation : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Transform _body;
    [SerializeField] Transform _text;

    public void Play()
    {
        _animProgress = 0f;
        _bodyRend.material.color = _highlightTextColor;
    }

    const float HIGHLIGHT_BODY_SCALE_MULT = 1.3f;
    const float HIGHLIGHT_TEXT_SCALE_MULT = 1.6f;
    const float ANIMATION_DURATION = 1f;
    const float DEFAULT_SCALE_MULT = 1f;


    Vector3 _defaultBodyScale;
    Vector3 _defaultTextScale;
    float _animProgress;
    MeshRenderer _bodyRend;
    Color _defaultTextColor;
    Color _highlightTextColor;
    private void Awake()
    {
        _defaultBodyScale = _body.localScale;
        _defaultTextScale = _text.localScale;

        _bodyRend = _body.GetComponent<MeshRenderer>();
        _defaultTextColor = _bodyRend.sharedMaterial.color;
        _highlightTextColor = Color.Lerp(_defaultTextColor, Color.white, 0.5f);

        _animProgress = 1f;
    }

    void Update()
    {
        if (_animProgress < 1f)
        {
            _animProgress += Time.deltaTime / ANIMATION_DURATION;
            var bodyScaleMult = Mathf.Lerp(HIGHLIGHT_BODY_SCALE_MULT, DEFAULT_SCALE_MULT, _animProgress);
            _body.localScale = _defaultBodyScale * bodyScaleMult;

            var textScaleMult = Mathf.Lerp(HIGHLIGHT_TEXT_SCALE_MULT, DEFAULT_SCALE_MULT, _animProgress);
            _text.localScale = _defaultTextScale * textScaleMult;

            var color = Color.Lerp(_bodyRend.material.color, _defaultTextColor, _animProgress);
            _bodyRend.material.color = color;
        }
    }

}
