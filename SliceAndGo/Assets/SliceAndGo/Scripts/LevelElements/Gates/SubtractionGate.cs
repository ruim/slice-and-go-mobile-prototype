using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubtractionGate : RemoverGateBase
{

    [SerializeField] int _subtract;

    protected override void DoBallEnterAction(Ball ball)
    {
        RemoveBall(ball);
        _subtract--;
        _text.text = GetText();
        if (_subtract == 0)
        {
            DeactivateGate();
        }
    }

    protected override string GetText()
    {
        return "-" + _subtract.ToString();
    }

    Text _text;

    protected override void Awake()
    {
        base.Awake();
        _text = GetComponentInChildren<Text>();
    }
}
