using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DivisionGate : RemoverGateBase
{
    [SerializeField] int _division;

    protected override void DoBallEnterAction(Ball ball)
    {
        _counter++;
        if (_counter % _division != 0)
        {
            RemoveBall(ball);
        }
    }

    protected override string GetText()
    {
        return "÷" + _division.ToString();
    }

    int _counter;
}
