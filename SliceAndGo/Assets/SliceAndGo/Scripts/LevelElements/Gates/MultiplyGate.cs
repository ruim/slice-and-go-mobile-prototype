using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

public class MultiplyGate : SpawnerGateBase
{
    [Header("Properties")]
    [SerializeField] int _multiply = 2;


    protected override void DoBallEnterAction(Ball ball)
    {
        var toSpawn = _multiply - 1;
        SpawnBalls(toSpawn);
    }

    protected override string GetText()
    {
        return "x" + _multiply.ToString();
    }

}
