using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GateEvents
{
    public delegate void SpawnBallDelegate(SpawnerGateBase sender, Vector3 pos);

    public static event SpawnBallDelegate onShouldSpawnBall;

    public static void SendShouldSpawnBall(SpawnerGateBase sender, Vector3 pos)
    {
        onShouldSpawnBall?.Invoke(sender, pos);
    }

    public delegate void RemoveBallDelegate(RemoverGateBase sender, Ball ball);

    public static event RemoveBallDelegate onBallWillBeRemoved;

    public static void SendBallWillBeRemoved(RemoverGateBase sender, Ball ball) => onBallWillBeRemoved?.Invoke(sender, ball);

}
