using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpawnerGateBase : GateBase
{

    protected void SpawnBalls(int amount)
    {
        for (var i = 0; i < amount; i++)
        {
            var worldPos = GetSpawnPos();
            GateEvents.SendShouldSpawnBall(this, worldPos);
        }
    }

    Vector3 GetSpawnPos()
    {
        var size = boxCol.size;
        var x = Random.Range(-0.5f, 0.5f) * size.x;
        var bottom = -size.y * 0.5f;
        var y = Random.Range(bottom - 1f, bottom);
        return transform.localToWorldMatrix.MultiplyPoint3x4(new Vector3(x, y, 0f));
    }
}
