using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatePositionLoop : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Transform _positionA;
    [SerializeField] Transform _positionB;

    //these gates will also stop the position loop
    [SerializeField] GateBase[] _additionalStoppers;

    [Header("Properties")]
    [SerializeField] float _speed;

    [Header("DEBUG")]
    [SerializeField, Range(0f, 1f)] float _progress;

    const float DIRECTION_RIGHT = 1f;
    const float DIRECTION_LEFT = -1;

    GateBase _target;
    float _distance;
    float _direction;

    List<GateBase> _stoppers = new List<GateBase>();

    private void Awake()
    {
        _target = GetComponentInChildren<GateBase>();
        _distance = Vector3.Distance(_positionA.position, _positionB.position);
        _progress = Vector3.Distance(_target.transform.position, _positionA.position) / _distance;
        _direction = DIRECTION_RIGHT;

        _stoppers.Add(_target);
        foreach (var gate in _additionalStoppers)
        {
            _stoppers.Add(gate);
        }


    }

    private void Start()
    {
        foreach (var gate in _stoppers)
        {
            gate.onGateEnteredFirstTime += OnGateEnteredFirstTime;
            gate.onGateWillBeDeactivated += OnGateWillBeDeactivated;
        }
    }

    private void FixedUpdate()
    {
        var duration = _distance / _speed;
        _progress += (Time.fixedDeltaTime / duration) * _direction;
        if (_direction > 0)
        {
            if (_progress > 1f)
            {
                _progress = 1f;
                _direction = DIRECTION_LEFT;
            }
        }
        else if (_progress < 0f)
        {
            _progress = 0f;
            _direction = DIRECTION_RIGHT;
        }
        ApplyPosition();
    }

    void ApplyPosition()
    {
        _target.transform.position = Vector3.Lerp(_positionA.position, _positionB.position, _progress);
    }

    void OnGateEnteredFirstTime(GateBase gate)
    {
        Clear();
        enabled = false;
    }

    void OnGateWillBeDeactivated(GateBase gate)
    {
        if (gate == _target)
        {
            Clear();
            enabled = false;
        }
        else
        {
            _stoppers.Remove(gate);
        }
    }

    void Clear()
    {
        foreach (var gate in _stoppers)
        {
            gate.onGateEnteredFirstTime -= OnGateEnteredFirstTime;
            gate.onGateWillBeDeactivated -= OnGateWillBeDeactivated;
        }
    }

    private void OnDrawGizmos()
    {
        if (_positionA == null || _positionB == null)
        {
            return;
        }
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(_positionA.position, 0.2f);
        Gizmos.DrawWireSphere(_positionB.position, 0.2f);
        Gizmos.DrawLine(_positionA.position, _positionB.position);
    }

    private void OnValidate()
    {
        if (_positionA == null || _positionB == null)
        {
            return;
        }
        if (_target == null)
        {
            _target = GetComponentInChildren<GateBase>();
            if (_target == null)
            {
                return;
            }
        }
        ApplyPosition();
    }

}
