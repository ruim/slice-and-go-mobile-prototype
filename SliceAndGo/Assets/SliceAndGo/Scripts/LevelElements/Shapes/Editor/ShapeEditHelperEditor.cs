using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(ShapeEditHelper))]
public class ShapeEditHelperEditor : Editor
{
    // Start is called before the first frame update
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GUILayout.Space(20f);
        GUILayout.Label("Control");

        var shapeEditHelper = (ShapeEditHelper)target;

        _liveUpdate = GUILayout.Toggle(_liveUpdate, "Live Update");
        shapeEditHelper.SetLiveUpdate(_liveUpdate);

        _snapToGrid = GUILayout.Toggle(_snapToGrid, "Snap to Grid");
        shapeEditHelper.SnapPolygonToGrid(_snapToGrid);

        if (GUILayout.Button("Apply Mesh"))
        {
            shapeEditHelper.ApplyMesh();
        }

        if (GUILayout.Button("Create Mesh Asset"))
        {
            shapeEditHelper.CreateMeshAsset();
        }

        if (GUILayout.Button("Duplicate Mesh Asset"))
        {
            shapeEditHelper.DuplicateMeshAsset();
        }
        if (GUILayout.Button("Apply Curvy Spline"))
        {
            shapeEditHelper.ApplyPolygonFromCurvySpline();
        }
    }

    bool _liveUpdate;
    bool _snapToGrid;

}
