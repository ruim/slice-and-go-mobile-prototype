using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;


#if UNITY_EDITOR
[ExecuteInEditMode]
[RequireComponent(typeof(ShapeBase))]
public class ShapeEditHelper : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] float _gridSize = 0.5f;

    public void ApplyMesh()
    {
        DestroyOldMeshAssetIfExists();
        var shape = GetComponent<ShapeBase>();
        shape.ApplyMesh();
    }

    public void CreateMeshAsset()
    {
        if (_liveUpdate)
        {
            _liveUpdate = false;
        }

        var mf = GetMeshFilter();
        if (mf.sharedMesh == null)
        {
            Debug.Log("Cannot save mesh because mesh is NULL", gameObject);
            return;
        }
        SaveMeshAsset();
    }

    public void SetLiveUpdate(bool liveUpdate) => _liveUpdate = liveUpdate;

    public void SnapPolygonToGrid(bool snap) => _snapPolygonToGrid = snap;

    public void DuplicateMeshAsset()
    {
        var mf = GetMeshFilter();
        if (mf.sharedMesh == null)
        {
            Debug.Log("cannot duplicate mesh asset because mesh is NULL");
            return;
        }
        mf.sharedMesh = Instantiate<Mesh>(mf.sharedMesh);
        SaveMeshAsset();
    }

    public void ApplyPolygonFromCurvySpline()
    {
        var spline = GetComponentInChildren<CurvySpline>();
        if (spline == null)
        {
            Debug.Log("Cannot apply polygon from Curvy because spline is null");
            return;
        }


        var splinePoints = spline.GetApproximation();
        var path = new Vector2[splinePoints.Length];
        for (var i = 0; i < splinePoints.Length; i++)
        {
            path[i] = (Vector2)splinePoints[i];
        }

        var polygon = GetComponent<PolygonCollider2D>();
        polygon.SetPath(0, path);

        ApplyMesh();
    }

    class PolyData
    {
        const float TOLLERANCE = 0.2f;

        public List<Vector2> path;

        public PolyData(PolygonCollider2D polygon)
        {
            path = new List<Vector2>(polygon.GetPath(0));
        }

        public bool IsMatch(PolygonCollider2D polygon)
        {
            var polyPath = polygon.GetPath(0);
            if (polyPath.Length != path.Count)
            {
                return false;
            }
            for (var i = 0; i < path.Count; i++)
            {
                var diff = path[i] - polyPath[i];
                if (diff.sqrMagnitude > TOLLERANCE * TOLLERANCE)
                {
                    return false;
                }
            }
            return true;
        }

        public void Set(PolygonCollider2D polygon)
        {
            path.Clear();
            foreach (var pt in polygon.GetPath(0))
            {
                path.Add(pt);
            }
        }
    }

    const string PARENT_FOLDER = "Assets/SliceAndGo/Meshes";
    const string SLICEABLE_SHAPE_PREFIX = "Sliceable";
    const string STATIC_SHAPE_PREFIX = "Static";

    bool _liveUpdate;
    bool _snapPolygonToGrid;

    MeshFilter _meshFilterCache;
    PolyData _polyData;

    void Update()
    {
        if (_liveUpdate)
        {
            var polygon = GetComponent<PolygonCollider2D>();
            if (_polyData == null)
            {
                _polyData = new PolyData(polygon);
            }
            if (_polyData.IsMatch(polygon) == false)
            {
                _polyData.Set(polygon);
                ApplyMesh();
            }
        }

        if (_snapPolygonToGrid)
        {
            //   _snapPolygonToGrid = false;
            var polygon = GetComponent<PolygonCollider2D>();
            var path = polygon.GetPath(0);
            for (var i = 0; i < path.Length; i++)
            {
                var pt = path[i];
                pt.x = Mathf.Round(pt.x / _gridSize) * _gridSize;
                pt.y = Mathf.Round(pt.y / _gridSize) * _gridSize;
                path[i] = pt;
            }
            polygon.SetPath(0, path);
        }
    }

    string GetFolderName() => gameObject.scene.name;

    string CreateOrGetExistingFolder()
    {
        var folderName = GetFolderName();
        var folderPath = PARENT_FOLDER + "/" + folderName;
        var folderExists = AssetDatabase.IsValidFolder(folderPath);
        if (folderExists == false)
        {
            AssetDatabase.CreateFolder(PARENT_FOLDER, folderName);
        }
        return folderPath;
    }

    void DestroyOldMeshAssetIfExists()
    {
        var mf = GetMeshFilter();
        if (mf.sharedMesh != null && AssetDatabase.Contains(mf.sharedMesh))
        {
            DestroyImmediate(mf.sharedMesh, true);
            mf.sharedMesh = null;
            AssetDatabase.SaveAssets();
        }
    }

    void SaveMeshAsset()
    {
        var folderPath = CreateOrGetExistingFolder();
        var assetPath = folderPath + "/" + GenerateAssetFileName();
        AssetDatabase.CreateAsset(GetMeshFilter().sharedMesh, assetPath);
        AssetDatabase.SaveAssets();
    }

    string GenerateAssetFileName()
    {
        var filePrefix = "";
        var shape = GetComponent<ShapeBase>();
        switch (shape.shapeType)
        {
            case ShapeBase.ShapeType.Sliceable:
                filePrefix = SLICEABLE_SHAPE_PREFIX;
                break;
            case ShapeBase.ShapeType.Static:
                filePrefix = STATIC_SHAPE_PREFIX;
                break;
        }

        return filePrefix + GUID.Generate() + ".asset";
    }

    MeshFilter GetMeshFilter()
    {
        if (_meshFilterCache == null)
        {
            _meshFilterCache = GetComponentInChildren<MeshFilter>();
        }
        return _meshFilterCache;
    }
}

#endif