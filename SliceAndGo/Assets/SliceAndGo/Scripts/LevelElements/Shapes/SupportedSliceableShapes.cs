using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//a group of shapes that are supporting each other
//when one gets sliced they all get released
//usefull to simulate physics and shapes in weird positions
public class SupportedSliceableShapes : MonoBehaviour
{
    List<SliceableShape> _shapes = new List<SliceableShape>();

    private void Awake()
    {
        GetComponentsInChildren(_shapes);
    }

    private void Start()
    {
        foreach (var shape in _shapes)
        {
            shape.onSliced += OnShapeSliced;
            shape.onShapeWillRemove += OnShapeWillRemove;
        }
    }


    void OnShapeSliced(SliceableShape sliced, SliceableShape positiveSliced, SliceableShape negativeSliced)
    {
        foreach (var shape in _shapes)
        {
            shape.onShapeWillRemove -= OnShapeWillRemove;
            shape.onSliced -= OnShapeSliced;
            if (shape != sliced)
            {
                shape.Release();
            }
        }
        _shapes.Clear();
    }
    void OnShapeWillRemove(SliceableShape shape)
    {
        _shapes.Remove(shape);
    }
}
