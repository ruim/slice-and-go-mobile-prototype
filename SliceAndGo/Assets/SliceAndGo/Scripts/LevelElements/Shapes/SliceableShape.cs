using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]
public class SliceableShape : ShapeBase
{

    [Header("References")]
    [SerializeField] GameObject _attachmentPointsParent;

    [Header("Properties")]
    [SerializeField] float _fadeOutAndDestroyDuration = 1f;
    [SerializeField] float _minAreaToDestroy = 4f;
    [SerializeField] string _removedLayerName = "RemovedShapes";
    [SerializeField] bool _releaseOnStart;

    public static event System.Action<SliceableShape> onFirstShapeSliced;

    public delegate void SlicedDelegate(SliceableShape sliced, SliceableShape positiveSliced, SliceableShape negativeSliced);

    public event SlicedDelegate onSliced;

    public event System.Action<SliceableShape> onShapeWillRemove;

    public override ShapeType shapeType => ShapeType.Sliceable;

    public PolygonCollider2D polygon { get; private set; }

    public bool hasBeenReleased { get; private set; }

    public List<AttachmentPoint> attachmentPoints { get; private set; } = new List<AttachmentPoint>();

    [Header("Debug")]
    [SerializeField] float _area;

    public float removeCountdown { get; private set; }

    public bool IsCountingDownToRemove() => removeCountdown > 0f;

    public void SetupAfterSliced(List<Vector2> slicedShape, SliceableShape parentShape)
    {
        var shapeCenter = Vector2.zero;
        foreach (var pt in slicedShape)
        {
            shapeCenter += pt;
        }
        shapeCenter /= slicedShape.Count;

        for (var i = 0; i < slicedShape.Count; i++)
        {
            slicedShape[i] = slicedShape[i] - shapeCenter;
        }

        transform.position = parentShape.transform.position;
        transform.rotation = parentShape.transform.rotation;
        var worldCenter = transform.localToWorldMatrix.MultiplyPoint3x4(shapeCenter);
        transform.position = worldCenter;

        polygon.SetPath(0, slicedShape);
        ApplyMesh();

        _area = CalcArea();
        if (_area < _minAreaToDestroy)
        {
            StartCoroutine(RemoveRoutine());
        }

        foreach (var attachPt in parentShape.attachmentPoints)
        {
            attachmentPoints.Add(attachPt);
        }

        if (parentShape.hasBeenReleased)
        {
            Release();
        }
        else
        {
            StartCoroutine(ReleaseIfNotAttachedRoutine());
        }

        var parentMaterial = parentShape.shapeRenderer.meshRenderer.sharedMaterial;
        shapeRenderer.ApplySharedMaterial(parentMaterial);
    }

    public void RemoveWithDelay(float delay)
    {
        removeCountdown = delay;
    }

    public void ApplySliced(SliceableShape positiveSlice, SliceableShape negativeSlice)
    {
        if (_anyShapeSliced == false)
        {
            _anyShapeSliced = true;
            onFirstShapeSliced?.Invoke(this);
        }
        onSliced?.Invoke(this, positiveSlice, negativeSlice);
        Destroy(gameObject);
    }

    public void Release()
    {
        hasBeenReleased = true;
        _rb.isKinematic = false;
    }

    protected override bool ShouldUseWeirdPositionHackForMesh => false;

    Rigidbody2D _rb;

    bool _removing;

    static bool _anyShapeSliced = false;

    void Awake()
    {
        polygon = GetComponent<PolygonCollider2D>();
        _rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        _area = CalcArea();
        if (_releaseOnStart && hasBeenReleased == false)
        {
            Release();
        }

        //was having an issue where attachment points where being copied from the parent shape
        //and the list was being emptied on Start, so if the list is already full, we don't check for points
        if (attachmentPoints.Count == 0)
        {
            _attachmentPointsParent.GetComponentsInChildren(attachmentPoints);
            //remove points from inside this GO, so they don't get destroyed when this shape is removed
            foreach (var pt in attachmentPoints)
            {
                pt.transform.parent = transform.parent;
            }
        }

    }

    private void Update()
    {
        if (removeCountdown > 0f)
        {
            removeCountdown -= Time.deltaTime;
            if (removeCountdown < 0f)
            {
                StartCoroutine(RemoveRoutine());
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (_removing)
        {
            return;
        }
        if (other.attachedRigidbody.TryGetComponent(out BallCup ballCup))
        {
            StartCoroutine(RemoveRoutine());
        }
    }

    IEnumerator ReleaseIfNotAttachedRoutine()
    {
        //   yield return new WaitForSeconds(1f);
        yield return new WaitForFixedUpdate();
        if (IsAttached() == false)
        {
            Release();
        }
        yield return null;
    }

    bool IsAttached()
    {
        foreach (var pt in attachmentPoints)
        {
            var overlaps = polygon.OverlapPoint(pt.transform.position);
            if (overlaps)
            {
                return true;
            }
        }
        return false;
    }

    float GetDeterminant(Vector2 a, Vector2 b)
    {
        return a.x * b.y - b.x * a.y;
    }

    float CalcArea()
    {
        var path = polygon.GetPath(0);
        if (path.Length < 3)
        {
            return 0f;
        }
        float area = GetDeterminant(path[path.Length - 1], path[0]);
        for (int i = 1; i < path.Length; i++)
        {
            area += GetDeterminant(path[i - 1], path[i]);
        }
        return Mathf.Abs(area) * 0.5f;
    }

    IEnumerator RemoveRoutine()
    {
        _removing = true;
        onShapeWillRemove?.Invoke(this);
        gameObject.layer = LayerMask.NameToLayer(_removedLayerName);
        var startScale = transform.localScale;
        var endScale = Vector3.zero;
        shapeRenderer.Flash(0.5f);
        var progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / _fadeOutAndDestroyDuration;
            transform.localScale = Vector3.Lerp(startScale, endScale, progress);
            yield return null;
        }
        Destroy(gameObject);
    }
}
