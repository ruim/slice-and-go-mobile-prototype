using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachmentPoint : MonoBehaviour
{
    public Vector2 position { get; private set; }

    private void Awake()
    {
        position = (Vector2)transform.position;
    }
}
