using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class ShapeRenderer : MonoBehaviour
{
    [SerializeField] LineRenderer _optionalOutline;
    [SerializeField] float _outlineColorMult = 0.7f;

    public Mesh mesh { get; private set; }

    public MeshRenderer meshRenderer
    {
        get
        {
            if (_meshRendererCache == null)
            {
                _meshRendererCache = GetComponent<MeshRenderer>();
            }
            return _meshRendererCache;
        }
    }

    //for statis shapes the renderer position was offset for some weird reason....
    //for sliceable shapes the renderer is well positioned....
    //the useWeirdPositionHack allows to use/don't use the hack
    public void CreateMesh(PolygonCollider2D polygon, bool useWeirdPositionHack)
    {
        mesh = polygon.CreateMesh(false, false);
        mesh.hideFlags = HideFlags.None;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.Optimize();

        var meshFilter = GetComponent<MeshFilter>();
        meshFilter.sharedMesh = mesh;

        if (useWeirdPositionHack)
        {
            transform.localPosition = -transform.parent.position;
        }

        if (_optionalOutline != null)
        {
            var path = polygon.GetPath(0);

            _optionalOutline.positionCount = path.Length;
            for (var i = 0; i < path.Length; i++)
            {
                _optionalOutline.SetPosition(i, (Vector3)path[i]);
            }

            UpdateOutlineColor(meshRenderer.sharedMaterial.color);

            //the weird position hack is only for the mesh, the outline needs to be correctly aligned
            if (useWeirdPositionHack)
            {
                _optionalOutline.transform.localPosition = -transform.localPosition;
            }
        }
    }

    public void Flash(float duration)
    {
        StartCoroutine(FlashRoutine(duration));
    }

    public void ApplySharedMaterial(Material material)
    {
        meshRenderer.sharedMaterial = material;
        UpdateOutlineColor(material.color);
    }

    MeshRenderer _meshRendererCache;

    IEnumerator FlashRoutine(float duration)
    {
        var mat = meshRenderer.material;
        var endColor = mat.color;
        var startColor = Color.white;
        var progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / duration;
            mat.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }
    }

    void UpdateOutlineColor(Color baseColor)
    {
        if (_optionalOutline == null)
        {
            return;
        }
        var outlineColor = baseColor * _outlineColorMult;
        var colorKeys = new GradientColorKey[] {
            new GradientColorKey(outlineColor, 0f),
            new GradientColorKey(outlineColor, 1f)
        };
        var alphaKeys = new GradientAlphaKey[]{
            new GradientAlphaKey(1f, 0f),
            new GradientAlphaKey(1f, 1f)
        };
        var gradient = new Gradient();
        gradient.SetKeys(colorKeys, alphaKeys);
        _optionalOutline.colorGradient = gradient;
    }

}
