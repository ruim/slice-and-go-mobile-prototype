using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PolySlicer))]
public class SliceController : MonoBehaviour
{

    [Header("Properties")]
    [SerializeField] LayerMask _sliceableShapesLayerMask;

    [Header("References")]
    [SerializeField] SliceableShape _sliceableShapePrefab;
    public SliceLine sliceLine { get; private set; }

    public System.Action onValidTouchBegin;
    public System.Action onValidTouchEnd;


    public void ActivateWithLevel(LevelRoot currentLevel)
    {
        _currentLevel = currentLevel;
        _active = true;
        _validTouch = false;
    }

    public Vector2 TouchToSlicePoint()
    {
        var ray = _currentLevel.cameraController.cam.ScreenPointToRay(Input.mousePosition);
        if (_slicePlane.Raycast(ray, out var enter))
        {
            return ray.GetPoint(enter);
        }
        return Vector2.zero;
    }

    const float TOUCH_DRAG_TOLLERANCE = 0.01f;


    PolySlicer _slicer;
    bool _active;
    bool _validTouch;
    Plane _slicePlane;
    Vector2 _prevSlicePoint;
    LevelRoot _currentLevel;
    SliceableShape _currHitShape;

    void Awake()
    {
        _slicer = GetComponent<PolySlicer>();
        sliceLine = GetComponentInChildren<SliceLine>();
        _slicePlane = new Plane(Vector3.back, Vector3.zero);
    }

    void Update()
    {
        if (_active == false)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            _validTouch = true;
            _prevSlicePoint = TouchToSlicePoint();
            _slicer.ResetSliceData();
            onValidTouchBegin?.Invoke();
        }
        else if (Input.GetMouseButton(0))
        {
            if (_validTouch)
            {
                var currSlicePoint = TouchToSlicePoint();
                var diff = currSlicePoint - _prevSlicePoint;
                if (diff.sqrMagnitude > TOUCH_DRAG_TOLLERANCE * TOUCH_DRAG_TOLLERANCE)
                {
                    var shape = GetShapeInLine(_prevSlicePoint, currSlicePoint);
                    if (shape != _currHitShape)
                    {
                        if (_currHitShape != null)
                        {
                            var sliceStart = _prevSlicePoint - diff * 20f;
                            var sliceEnd = currSlicePoint + diff * 20f;
                            sliceLine.ResetLine(sliceStart);
                            sliceLine.SetEnd(sliceEnd);

                            var sliceSuccess = _slicer.ApplySlice(_currHitShape, sliceLine);
                            if (sliceSuccess)
                            {
                                var parent = _currHitShape.transform.parent;
                                var posShape = Instantiate(_sliceableShapePrefab, parent);
                                posShape.SetupAfterSliced(_slicer.positiveSliced, _currHitShape);

                                var negShape = Instantiate(_sliceableShapePrefab, parent);
                                negShape.SetupAfterSliced(_slicer.negativeSliced, _currHitShape);

                                _currHitShape.ApplySliced(posShape, negShape);

                                _slicer.ResetSliceData();
                            }
                        }
                        _currHitShape = shape;
                        _prevSlicePoint = currSlicePoint;
                    }
                    _prevSlicePoint = currSlicePoint;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (_validTouch)
                {
                    _validTouch = false;
                    onValidTouchEnd?.Invoke();
                }
            }
        }





        SliceableShape GetShapeInLine(Vector2 lineStart, Vector2 lineEnd)
        {
            var hit = Physics2D.Linecast(lineStart, lineEnd, _sliceableShapesLayerMask);
            if (hit.collider != null)
            {
                return hit.collider.GetComponent<SliceableShape>();
            }
            return null;
        }
    }
}
