using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PolySlicer : MonoBehaviour
{
    public List<Vector2> positiveSliced { get; private set; } = new List<Vector2>();
    public List<Vector2> negativeSliced { get; private set; } = new List<Vector2>();
    List<SliceEdge> _sliceEdges = new List<SliceEdge>();
    PolygonCollider2D _polygon;
    bool _validTouch;
    Plane _interactionPlane;

    public void ResetSliceData()
    {
        _sliceEdges.Clear();
        positiveSliced.Clear();
        negativeSliced.Clear();
    }

    public bool ApplySlice(SliceableShape shape, SliceLine sliceLine)
    {
        _polygon = shape.polygon;
        if (_polygon.pathCount == 0)
        {
            return false;
        }
        _sliceEdges.Clear();
        var sliceSuccess = false;

        var path = _polygon.GetPath(0);
        var toWorldMat = _polygon.transform.localToWorldMatrix;
        var toLocalMat = _polygon.transform.worldToLocalMatrix;
        for (var i = 0; i < path.Length; i++)
        {
            var ptA = toWorldMat.MultiplyPoint3x4(path[i]);
            var nextIndex = i + 1;
            if (nextIndex >= path.Length) //wrap around
            {
                nextIndex = 0;
            }
            var ptB = toWorldMat.MultiplyPoint3x4(path[nextIndex]);

            var direction = ptB - ptA;
            var ptsDist = (ptB - ptA).magnitude;

            var hit = Physics2D.Raycast(ptA, direction, ptsDist, sliceLine.layerMask);
            if (hit.collider != null)
            {
                var localHit = toLocalMat.MultiplyPoint3x4(hit.point);
                var sliceEdge = new SliceEdge(i, nextIndex, localHit);
                _sliceEdges.Add(sliceEdge);
                if (_sliceEdges.Count == 2)
                {
                    sliceSuccess = true;
                    break;
                }
            }
        }
        if (sliceSuccess)
        {
            var startEdge = _sliceEdges[0];
            var endEdge = _sliceEdges[1];

            positiveSliced.Clear();
            positiveSliced.Add(startEdge.localSlicePoint);
            AddPathPointsUntil(startEdge.nextVertex, endEdge.prevVertex, path, positiveSliced);

            positiveSliced.Add(endEdge.localSlicePoint);

            negativeSliced.Clear();
            negativeSliced.Add(endEdge.localSlicePoint);
            AddPathPointsUntil(endEdge.nextVertex, startEdge.prevVertex, path, negativeSliced);
            negativeSliced.Add(startEdge.localSlicePoint);
        }
        return sliceSuccess;
    }

    void Awake()
    {
        _interactionPlane = new Plane(Vector3.back, Vector3.zero);
    }

    /*
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _validTouch = true;
                var pt = TouchToSlicePoint();
                _lineSlice.ResetLine(pt);

            }
            else if (Input.GetMouseButton(0))
            {
                if (_validTouch)
                {
                    var pt = TouchToSlicePoint();
                    _lineSlice.SetEnd(pt);
                    if (ApplySlice())
                    {
                        _validTouch = false;
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _validTouch = false;
            }
        }*/

    void AddPathPointsUntil(int fromIndex, int untilIndex, Vector2[] path, List<Vector2> result)
    {
        var index = fromIndex;
        while (true)
        {
            var pt = path[index];
            result.Add(pt);

            if (index == untilIndex)
            {
                break;
            }
            index = (index + 1) % path.Length;
        }
    }

    void OnDrawGizmos()
    {
        if (_sliceEdges != null && _sliceEdges.Count > 0)
        {
            Gizmos.color = Color.magenta;
            foreach (var edge in _sliceEdges)
            {
                Gizmos.DrawWireSphere(edge.localSlicePoint, 0.1f);
            }
        }
        if (_polygon != null)
        {
            var prevMat = Gizmos.matrix;
            Gizmos.matrix = _polygon.transform.localToWorldMatrix;
            DrawPolyLineGizmos(positiveSliced, Color.green);
            DrawPolyLineGizmos(negativeSliced, Color.magenta);
            Gizmos.matrix = prevMat;
        }
    }

    void DrawPolyLineGizmos(List<Vector2> line, Color color)
    {
        if (line.Count < 2)
        {
            return;
        }
        Gizmos.color = color;
        for (var i = 0; i < line.Count - 1; i++)
        {
            var ptA = line[i];
            var ptB = line[i + 1];
            Gizmos.DrawLine(ptA, ptB);
        }
    }

}
