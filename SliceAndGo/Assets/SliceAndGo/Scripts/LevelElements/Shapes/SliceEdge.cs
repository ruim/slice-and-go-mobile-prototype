using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SliceEdge
{
    public int prevVertex;
    public int nextVertex;
    public Vector2 localSlicePoint;

    public SliceEdge(int prevVertex, int nextVertex, Vector2 localSlicePoint)
    {
        this.prevVertex = prevVertex;
        this.nextVertex = nextVertex;
        this.localSlicePoint = localSlicePoint;
    }
}
