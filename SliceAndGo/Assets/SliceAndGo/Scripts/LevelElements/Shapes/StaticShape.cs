using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StaticShape : ShapeBase
{
    public override ShapeType shapeType => ShapeType.Static;

    protected override bool ShouldUseWeirdPositionHackForMesh => true;

    void Start()
    {
        var rend = GetComponentInChildren<ShapeRenderer>();
        var poly = GetComponent<PolygonCollider2D>();
        rend.CreateMesh(poly, useWeirdPositionHack: true);
    }
}
