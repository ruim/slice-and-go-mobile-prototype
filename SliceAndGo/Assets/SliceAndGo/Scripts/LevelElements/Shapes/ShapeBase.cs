using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class ShapeBase : MonoBehaviour
{
    public enum ShapeType
    {
        Static, Sliceable
    }

    public abstract ShapeType shapeType { get; }
    public ShapeRenderer shapeRenderer
    {
        get
        {
            if (_shapeRenderer == null)
            {
                _shapeRenderer = GetComponentInChildren<ShapeRenderer>();
            }
            return _shapeRenderer;
        }
    }


    public void ApplyMesh()
    {
        var polygon = GetComponent<PolygonCollider2D>();
        shapeRenderer.CreateMesh(polygon, ShouldUseWeirdPositionHackForMesh);
    }

    protected abstract bool ShouldUseWeirdPositionHackForMesh { get; }

    ShapeRenderer _shapeRenderer;
}
