using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(EdgeCollider2D))]
public class SliceLine : MonoBehaviour
{
    const int NUM_LINE_POINTS = 2;

    EdgeCollider2D _edgeCollider;
    List<Vector2> _points = new List<Vector2>();

    public LayerMask layerMask { get; private set; }

    public Vector2 lineStart => _points[0];
    public Vector2 lineEnd => _points[1];

    public void ResetLine(Vector2 point)
    {
        _points[0] = point;
        _points[1] = point;
        _edgeCollider.SetPoints(_points);
    }

    public void SetEnd(Vector2 end)
    {
        _points[1] = end;
        _edgeCollider.SetPoints(_points);
    }

    void Awake()
    {
        for (var i = 0; i < NUM_LINE_POINTS; i++)
        {
            _points.Add(Vector2.zero);
        }
        _edgeCollider = GetComponent<EdgeCollider2D>();
        var layerName = LayerMask.LayerToName(gameObject.layer);
        layerMask = LayerMask.GetMask(layerName);
    }

    void OnrawGizmos()
    {
        if (_points != null && _points.Count == NUM_LINE_POINTS)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(_points[0], _points[1]);
            foreach (var pt in _points)
            {
                Gizmos.DrawWireSphere(pt, 0.1f);
            }
        }
    }

}
