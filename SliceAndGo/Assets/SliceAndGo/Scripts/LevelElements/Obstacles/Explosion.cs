using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Explosion : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Transform _collider;
    [SerializeField] GameObject _vfx;

    [Header("Properties")]
    [SerializeField] float _duration;
    [SerializeField] float _radius = 6f;
    [SerializeField] float _force = 10f;
    [SerializeField] LayerMask _explosionReactionLayers;

    bool _explosionColliderFinished = false;
    int _numVFXFinished = 0;
    List<ParticleSystem> _particleSystems = new List<ParticleSystem>();

    private IEnumerator Start()
    {
        GetComponent<Rigidbody2D>().isKinematic = true;
        _vfx.SetActive(true);
        _vfx.GetComponentsInChildren(_particleSystems);

        var hits = Physics2D.OverlapCircleAll(transform.position, _radius, _explosionReactionLayers);
        foreach (var col in hits)
        {
            if (col.TryGetComponent(out Ball ball))
            {
                Destroy(ball.gameObject);
            }
            else if (col.TryGetComponent(out Spikes spikes))
            {
                Destroy(spikes.gameObject);
            }
        }

        _collider.gameObject.SetActive(true);
        yield return new WaitForSeconds(_duration);
        _collider.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (_explosionColliderFinished)
        {
            var allVFXFinished = true;
            foreach (var ps in _particleSystems)
            {
                if (ps.IsAlive())
                {
                    allVFXFinished = false;
                    break;
                }
            }
            if (allVFXFinished)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Ball ball))
        {
            var diff = (Vector2)(ball.transform.position - transform.position);
            var force = diff.normalized * _force;
            ball.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
        }
    }

    private void OnDrawGizmos()
    {
        if (_radius > Mathf.Epsilon)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _radius);
        }
    }

}
