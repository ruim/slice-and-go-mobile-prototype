using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BombEvents
{
    public static event System.Action<Bomb> onBombExploded;

    public static void SendBombExploded(Bomb bomb) => onBombExploded?.Invoke(bomb);
}
