using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Ruim.Utils;
using Cinemachine;

[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Bomb : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] Color _expandColor = Color.red;
    [SerializeField] LayerMask _triggerLayers;

    [Header("References")]
    [SerializeField] Explosion _explosionPrefab;
    [SerializeField] Transform _body;

    [Header("DEBUG")]
    [SerializeField] bool _debugExplode;



    const string MATERIAL_COLOR_NAME = "_BaseColor";
    float EXPLODE_ANIM_DURATION = 2f;
    const float DEFAULT_SCALE_MULT = 1f;
    const float SHAKE_AMOUNT = 0.2f;
    float EXPAND_SCALE_MULT = 1.1f;


    bool _exploding;
    bool _canExplode = true;
    List<MeshRenderer> _renderers = new List<MeshRenderer>();

    private void Awake()
    {
        _body.GetComponentsInChildren(_renderers);
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (_debugExplode)
        {
            _debugExplode = false;
            Explode();
        }
#endif
    }

    void Explode()
    {
        StartCoroutine(ExpandRoutine());
        StartCoroutine(ExplodeRoutine());
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (_exploding || _canExplode == false)
        {
            return;
        }
        if (LayerUtils.LayerInMask(other.gameObject.layer, _triggerLayers))
        {
            Explode();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (LayerUtils.LayerInMask(other.gameObject.layer, _triggerLayers))
        {
            Explode();
            return;
        }
        if (other.attachedRigidbody.TryGetComponent(out BallCup ballCup))
        {
            StartCoroutine(DeactivateAndRemoveRoutine());
        }
    }

    IEnumerator ExplodeRoutine()
    {
        _exploding = true;
        var counter = 0f;
        while (counter < EXPLODE_ANIM_DURATION)
        {
            counter += Time.deltaTime;

            //shake
            var x = Random.Range(-0.5f, 0.5f) * SHAKE_AMOUNT;
            var y = Random.Range(-0.5f, 0.5f) * SHAKE_AMOUNT;
            _body.localPosition = new Vector3(x, y, 0f);

            yield return null;
        }
        Instantiate(_explosionPrefab, transform.position, transform.rotation, transform.parent);
        BombEvents.SendBombExploded(this);
        Destroy(gameObject);
    }

    IEnumerator ColorFromTo(Color from, Color to, float duration)
    {
        var block = new MaterialPropertyBlock();
        var progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / duration;
            var col = Color.Lerp(from, to, progress);
            block.SetColor(MATERIAL_COLOR_NAME, col);
            foreach (var rend in _renderers)
            {
                rend.SetPropertyBlock(block);
            }
            yield return null;
        }
    }

    IEnumerator ExpandRoutine()
    {
        var block = new MaterialPropertyBlock();
        var rends = _body.GetComponentsInChildren<MeshRenderer>();
        var startColor = rends[0].sharedMaterial.color;
        var defaultScale = _body.localScale;
        var progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / EXPLODE_ANIM_DURATION;

            //apply color
            var color = Color.Lerp(startColor, _expandColor, progress);
            block.SetColor(MATERIAL_COLOR_NAME, color);
            foreach (var rend in rends)
            {
                rend.SetPropertyBlock(block);
            }

            //TODO apply scale
            var scaleMult = Mathf.Lerp(DEFAULT_SCALE_MULT, EXPAND_SCALE_MULT, progress);
            _body.localScale = defaultScale * scaleMult;

            yield return null;
        }
    }

    IEnumerator DeactivateAndRemoveRoutine()
    {
        _canExplode = false;
        var defaultColor = _renderers[0].sharedMaterial.color;
        StartCoroutine(ColorFromTo(Color.white, defaultColor, 0.5f));

        var removeDuration = 1f;
        var progress = 0f;
        var startScale = transform.localScale;
        while (progress < 1f)
        {
            progress += Time.deltaTime / removeDuration;
            transform.localScale = Vector3.Lerp(startScale, Vector3.zero, progress);
            yield return null;
        }
        Destroy(gameObject);
    }



}
