using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallTrail : MonoBehaviour
{

    [SerializeField] LineRenderer _lineRend;

    public void SetActive(bool active)
    {
        if (active)
        {
            var pos = transform.position;
            for (var i = 0; i < _trailPositions.Length; i++)
            {
                _trailPositions[i] = pos;
            }
            _lineRend.SetPositions(_trailPositions);
            StartCoroutine(ActivateLineRendererWithFrameDelay());
        }
        else
        {
            _lineRend.enabled = false;
        }
    }

    const int TRAIL_SIZE = 3;
    const float TRAIL_LERP_AMOUNT = 10f;

    bool _active;
    Vector3[] _trailPositions = new Vector3[TRAIL_SIZE];

    private void Awake()
    {
        _lineRend.positionCount = TRAIL_SIZE;
    }

    private void FixedUpdate()
    {
        if (_lineRend.enabled == false)
        {
            return;
        }

        var delta = Time.fixedDeltaTime;
        for (var i = 0; i < _trailPositions.Length - 1; i++)
        {
            var next = _trailPositions[i + 1];
            _trailPositions[i] = Vector3.Lerp(_trailPositions[i], next, delta * TRAIL_LERP_AMOUNT);
        }
        _trailPositions[_trailPositions.Length - 1] = transform.position;

        _lineRend.SetPositions(_trailPositions);
    }

    IEnumerator ActivateLineRendererWithFrameDelay()
    {
        yield return null;
        _lineRend.enabled = true;
    }


}
