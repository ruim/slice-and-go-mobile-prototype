using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BallCupBallEnterAnim))]
[RequireComponent(typeof(BallCupConfettiAnim))]
public class BallCup : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Transform _body;

    public System.Action<Ball> onBallEnter;

    public Vector3 GetTriggerStart()
    {
        var local = _edgeCollider.points[0];
        return transform.localToWorldMatrix.MultiplyPoint3x4(local);
    }
    public Vector3 GetTriggerEnd()
    {
        var local = _edgeCollider.points[1];
        return transform.localToWorldMatrix.MultiplyPoint3x4(local);
    }

    public void PlayConfettiAnim()
    {
        _ballEnterAnim.Stop();
        _confettiAnim.Play();
    }

    EdgeCollider2D _edgeCollider;
    BallCupBallEnterAnim _ballEnterAnim;
    BallCupConfettiAnim _confettiAnim;

    private void Awake()
    {
        _edgeCollider = GetComponentInChildren<EdgeCollider2D>();
        _ballEnterAnim = GetComponent<BallCupBallEnterAnim>();
        _confettiAnim = GetComponent<BallCupConfettiAnim>();
    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Ball ball))
        {
            _ballEnterAnim.Play();
            onBallEnter?.Invoke(ball);
        }
    }

}