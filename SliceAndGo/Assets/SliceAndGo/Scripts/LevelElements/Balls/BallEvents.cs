using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BallEvents
{
    public static event System.Action<Ball> onBallCreated;

    public static void SendBallCreated(Ball ball) => onBallCreated?.Invoke(ball);

    public static event System.Action<Ball> onBallDestroyed;

    public static void SendBallDestroyed(Ball ball) => onBallDestroyed?.Invoke(ball);

}
