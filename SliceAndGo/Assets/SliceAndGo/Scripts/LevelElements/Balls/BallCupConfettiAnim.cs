using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCupConfettiAnim : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] Vector3 _spawnParticlesBodyScale = new Vector3(0.8f, 1.2f, 1f);
    [SerializeField] Vector3 _spawnParticlesBodyOffset = new Vector3(0.0f, 0.1f, 0f);
    [SerializeField] float _animDuration = 0.2f;
    [SerializeField] float _loopDuration = 5f;

    [Header("References")]
    [SerializeField] Transform _body;
    [SerializeField] ParticleSystem _vfx;

    [Header("DEBUG")]
    [SerializeField] bool _debugPlay;

    public void Play()
    {
        _progress = 0f;
        _loopCountdown = _loopDuration;
        _body.localScale = _spawnParticlesBodyScale;
        _body.localPosition = _spawnParticlesBodyOffset;
        if (_vfx.gameObject.activeInHierarchy == false)
        {
            _vfx.gameObject.SetActive(true);
        }
        _vfx.Play();
    }

    const float PROGRESS_FINISH = 1f;

    Vector3 _defaultBodyScale;
    Vector3 _defaultBodyPosition;
    float _progress = PROGRESS_FINISH;
    float _loopCountdown;

    private void Start()
    {
        _vfx.gameObject.SetActive(false);
        _defaultBodyPosition = _body.localPosition;
        _defaultBodyScale = _body.localScale;
    }

    private void Update()
    {
        if (_progress < PROGRESS_FINISH)
        {
            _progress += Time.deltaTime / _animDuration;
            _body.localScale = Vector3.Lerp(_spawnParticlesBodyScale, _defaultBodyScale, _progress);
            _body.localPosition = Vector3.Lerp(_spawnParticlesBodyOffset, _defaultBodyPosition, _progress);
        }
        if (_loopCountdown > 0f)
        {
            _loopCountdown -= Time.deltaTime;
            if (_loopCountdown < 0f)
            {
                Play();
            }
        }
    }

    private void OnValidate()
    {
        if (_debugPlay)
        {
            _debugPlay = false;
            Play();
        }
    }
}
