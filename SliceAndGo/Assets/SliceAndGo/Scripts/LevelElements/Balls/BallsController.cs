using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Ruim.Math;

public class BallsController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Ball _ballPrefab;
    [SerializeField] GameObject _spawnAreasParent;
    [SerializeField] Ball[] _initialBalls;

    [Header("Properties")]
    [SerializeField] float _spawnDuration = 1f;
    [SerializeField] RangeF _ballsSizeRange;
    [SerializeField] Gradient _ballColors;

    public BallCup ballCup { get; private set; }
    public int numBallsInCup { get; private set; }
    public int numBalls { get; private set; }
    public int numBallsToWin { get; private set; }


    public System.Action onProgressChanged;

    public System.Action onComplete;

    public System.Action onBallsFinishedSpawning;

    public System.Action onAllBallsRemoved;

    public float GetProgress01()
    {
        var progress = (float)numBallsInCup / (float)numBallsToWin;
        return Mathf.Clamp01(progress);
    }

    public void Init(int spawnAmount, int numBallsToWin)
    {
        this.numBallsToWin = numBallsToWin;
        if (spawnAmount == 0)
        {
            onBallsFinishedSpawning?.Invoke();
            return;
        }

        var spawnAreas = _spawnAreasParent.GetComponentsInChildren<BallsSpawnArea>();
        var amountPerArea = spawnAmount / spawnAreas.Length;

        foreach (var spawnArea in spawnAreas)
        {
            StartCoroutine(LevelInitSpawnBallsRoutine(amountPerArea, spawnArea));
        }

        StartCoroutine(CoroutineUtils.ExecuteWithDelay(_spawnDuration, () =>
        {
            onBallsFinishedSpawning?.Invoke();
        }));
    }

    bool _complete;


    private void Awake()
    {
        ballCup = GetComponentInChildren<BallCup>();
        numBalls = 0;
        GateEvents.onShouldSpawnBall += OnGateShouldSpawnBall;
        BallEvents.onBallCreated += OnBallCreated;
        BallEvents.onBallDestroyed += OnBallDestroyed;
    }

    private void Start()
    {
        ballCup.onBallEnter += OnBallEnterCup;

        foreach (var ball in _initialBalls)
        {
            SetupBall(ball, gameObject);
        }


    }

    void OnDestroy()
    {
        GateEvents.onShouldSpawnBall -= OnGateShouldSpawnBall;
        BallEvents.onBallCreated -= OnBallCreated;
        BallEvents.onBallDestroyed -= OnBallDestroyed;
    }

    Ball SpawnBall(Vector3 pos, GameObject spawner)
    {
        var ball = Instantiate(_ballPrefab, pos, Quaternion.identity, transform);
        SetupBall(ball, spawner);
        return ball;
    }

    void SetupBall(Ball ball, GameObject spawner)
    {
        var color = _ballColors.Evaluate(Random.value);
        ball.Setup(color, _ballsSizeRange.random, spawner);
    }

    void OnBallEnterCup(Ball ball)
    {
        Destroy(ball.gameObject);
        numBallsInCup++;
        onProgressChanged?.Invoke();
        if (_complete == false && numBallsInCup >= numBallsToWin)
        {
            _complete = true;
            onComplete?.Invoke();
        }
    }

    void OnGateShouldSpawnBall(SpawnerGateBase sender, Vector3 spawnPos)
    {
        var spawner = sender.gameObject;
        var ball = SpawnBall(spawnPos, spawner);
        ball.ApplySpawnAfterGate();
    }

    void OnBallCreated(Ball ball)
    {
        numBalls++;
    }

    void OnBallDestroyed(Ball ball)
    {
        numBalls--;
        if (numBalls == 0)
        {
            onAllBallsRemoved?.Invoke();
        }
    }

    IEnumerator LevelInitSpawnBallsRoutine(int amount, BallsSpawnArea spawnArea)
    {
        int spawnedSoFar = 0;
        float progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / _spawnDuration;
            int spawnTarget = (int)Mathf.Lerp(0, amount, progress);
            int toSpawn = spawnTarget - spawnedSoFar;

            for (int i = 0; i < toSpawn; i++)
            {
                var pos = spawnArea.GetRandomSpawnPosition();
                var ball = SpawnBall(pos, gameObject);
                ball.ApplyLevelInitSpawn();
            }
            spawnedSoFar = spawnTarget;
            yield return null;
        }
    }

}