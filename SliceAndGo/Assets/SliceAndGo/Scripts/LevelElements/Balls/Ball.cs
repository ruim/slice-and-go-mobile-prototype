using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BallTrail))]
public class Ball : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] float _spawnDuration;
    [SerializeField] float _spawnDrag = 10f;
    [SerializeField] float _maxDeformSpeed = 5f;


    [Header("References")]
    [SerializeField] Transform _body;

    public GameObject spawner { get; private set; }

    public void Setup(Color color, float scale, GameObject spawner)
    {
        this.spawner = spawner;
        SetColor(color);
        transform.localScale = Vector3.one * scale;
    }

    public void ApplyLevelInitSpawn()
    {
        StartCoroutine(LevelBeginSpawnRoutine());
    }

    public void ApplySpawnAfterGate()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.down * Random.Range(4f, 7f);
        StartCoroutine(SpawnAfterGateRoutine());
    }

    const float DEFAULT_DEFORM_MULT = 1f;
    const float SPEED_STRETCH = 1.3f;

    MeshRenderer _meshRenderer;
    MaterialPropertyBlock _block;
    BallTrail _trail;
    Rigidbody2D _rb;
    Vector3 _defaultBodyScale;

    void Awake()
    {
        _block = new MaterialPropertyBlock();
        _meshRenderer = GetComponentInChildren<MeshRenderer>();
        _trail = GetComponent<BallTrail>();
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        _defaultBodyScale = _body.localScale;
        BallEvents.SendBallCreated(this);
    }

    private void OnDestroy()
    {
        BallEvents.SendBallDestroyed(this);
    }

    private void FixedUpdate()
    {
        var vel = _rb.velocity;
        var speedSq = vel.sqrMagnitude;

        if (speedSq > Mathf.Epsilon)
        {
            var speed = Mathf.Sqrt(speedSq);
            var deformRatio = Mathf.Clamp01(speed / _maxDeformSpeed);

            var sx = Mathf.Lerp(_defaultBodyScale.x, _defaultBodyScale.x * SPEED_STRETCH, deformRatio);
            var sy = Mathf.Lerp(_defaultBodyScale.y, _defaultBodyScale.y / SPEED_STRETCH, deformRatio);
            var sz = _defaultBodyScale.z;
            var targetScale = new Vector3(sx, sy, sz);
            _body.localScale = Vector3.Lerp(_body.localScale, targetScale, 20f * Time.fixedDeltaTime);

            var angle = Mathf.Atan2(vel.y, vel.x) * Mathf.Rad2Deg;
            var targetRot = Quaternion.Euler(0f, 0f, angle);
            //_body.rotation = Quaternion.Slerp(_body.rotation, targetRot, 10f * delta);
            _body.rotation = targetRot;
        }
        else
        {
            _body.localScale = _defaultBodyScale;
        }
    }

    private void OnDrawGizmos()
    {
        if (_rb == null)
        {
            return;
        }
        var vel = _rb.velocity;
        if (vel.magnitude < 0.1f)
        {
            return;
        }
        Gizmos.color = Color.black;
        var lineEnd = transform.position + (Vector3)(vel.normalized * 10f);
        Gizmos.DrawLine(transform.position, lineEnd);
    }

    IEnumerator LevelBeginSpawnRoutine()
    {
        var endScale = transform.localScale;
        var startScale = Vector3.zero;
        transform.localScale = startScale;
        var rb = GetComponent<Rigidbody2D>();
        var defaultDrag = rb.drag;
        rb.drag = _spawnDrag;
        float progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / _spawnDuration;
            transform.localScale = Vector3.Lerp(startScale, endScale, progress);
            yield return null;
        }
        rb.drag = defaultDrag;
        _trail.SetActive(true);
    }

    IEnumerator SpawnAfterGateRoutine()
    {
        var endColor = _block.GetColor("_BaseColor");
        var startColor = Color.white;

        var rendTrans = _meshRenderer.transform;
        var endScale = rendTrans.localScale;
        var startScale = endScale * 2f;
        var progress = 0f;
        var duration = 0.5f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / duration;
            var col = Color.Lerp(startColor, endColor, progress);
            SetColor(col);
            rendTrans.localScale = Vector3.Lerp(rendTrans.localScale, endScale, progress);
            yield return null;
        }
        _trail.SetActive(true);
    }

    void SetColor(Color color)
    {
        _block.SetColor("_BaseColor", color);
        _meshRenderer.SetPropertyBlock(_block);
    }
}
