using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCupBallEnterAnim : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] Vector3 _ballEnterBodyScale = new Vector3(1.1f, 0.8f, 1f);
    [SerializeField] Vector3 _ballEnterBodyOffset = new Vector3(0f, -0.2f, 0f);
    [SerializeField] float _minProgressToReplay = 0.5f;
    [SerializeField] float _animDuration = 0.2f;

    [Header("References")]
    [SerializeField] Transform _body;

    public void Play()
    {
        if (_progress < _minProgressToReplay)
        {
            return;
        }
        _progress = PROGRESS_START;
        _body.localScale = _ballEnterBodyScale;
        _body.localPosition = _ballEnterBodyOffset;
    }

    public void Stop()
    {
        _progress = PROGRESS_FINISH;
    }

    const float PROGRESS_START = 0f;
    const float PROGRESS_FINISH = 1f;

    float _progress = PROGRESS_FINISH;
    Vector3 _defaultBodyScale;
    Vector3 _defaultBodyPosition;

    private void Awake()
    {
        _defaultBodyScale = _body.localScale;
        _defaultBodyPosition = _body.localPosition;
    }

    private void Update()
    {
        if (_progress < PROGRESS_FINISH)
        {
            _progress += Time.deltaTime / _animDuration;
            _body.localScale = Vector3.Lerp(_ballEnterBodyScale, _defaultBodyScale, _progress);
            _body.localPosition = Vector3.Lerp(_ballEnterBodyOffset, _defaultBodyPosition, _progress);
        }
    }
}
