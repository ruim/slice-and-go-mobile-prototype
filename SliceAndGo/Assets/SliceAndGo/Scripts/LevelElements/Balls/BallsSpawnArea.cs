using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallsSpawnArea : MonoBehaviour
{

    [SerializeField] Vector2 _spawnArea = new Vector2(2f, 1f);

    public Vector3 GetRandomSpawnPosition()
    {
        var x = Random.Range(-0.5f, 0.5f) * _spawnArea.x;
        var y = Random.Range(-0.5f, 0.5f) * _spawnArea.y;
        return transform.localToWorldMatrix.MultiplyPoint3x4(new Vector3(x, y, 0f));
    }

    private void OnDrawGizmos()
    {
        var prevMat = Gizmos.matrix;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.yellow;
        var size = (Vector3)_spawnArea;
        size.z = 1f;
        Gizmos.DrawWireCube(Vector3.zero, size);
        Gizmos.matrix = prevMat;
    }
}
