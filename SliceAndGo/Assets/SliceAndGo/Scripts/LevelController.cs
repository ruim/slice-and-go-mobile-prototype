using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class LevelController : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] int _numInitialBalls = 5;
    [SerializeField] int _minBallsToWin = 3;

    [Header("References")]
    [SerializeField] BallsController _ballsController;

    private void Start()
    {
        _ballsController.Init(_numInitialBalls, _minBallsToWin);
    }

}
