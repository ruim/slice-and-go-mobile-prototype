using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class TouchTrailRender : MonoBehaviour
{
    [Header("References")]
    [SerializeField] SliceController _sliceController;

    [Header("Properties")]
    [SerializeField] int _trailSize;

    public void SetActive(bool active)
    {
        _active = active;
        _validTouch = false;
    }

    LineRenderer _renderer;
    Vector3[] _points;
    bool _validTouch;
    bool _active;

    void Awake()
    {
        _points = new Vector3[_trailSize];
        _renderer = GetComponent<LineRenderer>();
        _renderer.positionCount = _points.Length;
    }

    void Start()
    {
        _renderer.enabled = false;
    }

    void Update()
    {
        if (_active == false)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            var pt = _sliceController.TouchToSlicePoint();
            for (var i = 0; i < _points.Length; i++)
            {
                _points[i] = pt;
            }
            ApplyLine();
            _renderer.enabled = true;
            _validTouch = true;
        }
        else if (Input.GetMouseButton(0))
        {
            if (_validTouch)
            {
                for (var i = 0; i < _points.Length - 1; i++)
                {
                    _points[i] = _points[i + 1];
                }
                var pt = _sliceController.TouchToSlicePoint();
                _points[_points.Length - 1] = pt;
                ApplyLine();
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _validTouch = false;
            _renderer.enabled = false;
        }
    }

    void ApplyLine()
    {
        _renderer.positionCount = _points.Length;
        for (var i = 0; i < _points.Length; i++)
        {
            _renderer.SetPosition(i, _points[i]);
        }
    }
}
