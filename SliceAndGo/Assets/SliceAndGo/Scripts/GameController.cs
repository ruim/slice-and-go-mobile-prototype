using System.Collections;
using System.Collections.Generic;
using HomaGames.HomaBelly;
using Ruim.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(LevelScenesList))]
public class GameController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] ScreenController _screenController;
    [SerializeField] SliceController _sliceController;
    [SerializeField] TouchTrailRender _touchTrailRender;

    enum State
    {
        Playing, LevelWin, LevelLose, WaitingToFillCup
    }

    const string LAST_LEVEL_KEY = "last_level";
    const int LAST_LEVEL_NOT_FOUND_VALUE = -1;
    const int FIRST_LEVEL_VALUE = 1;

    const float MAX_STUCK_DURATION = 3f;

    int _currentLevelNumber;
    State _state;
    LevelRoot _currentLevel;
    float _stuckCountdown;

    static bool _firstGameplay = true;

    private void Start()
    {
        _currentLevelNumber = PlayerPrefs.GetInt(LAST_LEVEL_KEY, LAST_LEVEL_NOT_FOUND_VALUE);
        if (_currentLevelNumber == LAST_LEVEL_NOT_FOUND_VALUE || _currentLevelNumber < FIRST_LEVEL_VALUE)
        {
            _currentLevelNumber = FIRST_LEVEL_VALUE;
        }
        SceneManager.sceneLoaded += OnSceneLoaded;
        BeginCurrentLevel();
    }

    void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void Update()
    {
        switch (_state)
        {
            case State.LevelWin:
                if (Input.GetMouseButtonDown(0))
                {
                    //increase level number and save
                    _currentLevelNumber++;
                    PlayerPrefs.SetInt(LAST_LEVEL_KEY, _currentLevelNumber);
                    PlayerPrefs.Save();
                    ReloadScene();
                }
                break;
            case State.LevelLose:
                if (Input.GetMouseButtonDown(0))
                {
                    ReloadScene();
                }
                break;
            case State.WaitingToFillCup:
                var lastSection = _currentLevel.flowController.lastSection;
                if (lastSection.hasBallsInTrigger == false)
                {
                    _state = State.LevelWin;
                    _currentLevel.confettiVFX.SetActive(true);
                    CoroutineUtils.ExecuteWithDelay(this, 1f, () =>
                    {
                        _currentLevel.ballsController.ballCup.PlayConfettiAnim();
                        _screenController.ShowScreen<LevelWinScreen>();
                        var levelWinScreen = _screenController.GetScreen<LevelWinScreen>();
                        levelWinScreen.Setup(_currentLevelNumber);
                    });
                }

                break;
        }
    }

    void ReloadScene()
    {
        string currScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currScene);
    }

    private void BeginCurrentLevel()
    {
        var levelSceneName = GetComponent<LevelScenesList>().GetLevelSceneName(_currentLevelNumber);
        SceneManager.LoadScene(levelSceneName, LoadSceneMode.Additive);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (_currentLevelNumber < FIRST_LEVEL_VALUE)
        {
            return;
        }
        var levelSceneName = GetComponent<LevelScenesList>().GetLevelSceneName(_currentLevelNumber);
        if (string.Compare(scene.name, levelSceneName) != 0)
        {
            return;
        }
        _currentLevel = GetLevelRootInScene(scene);
        _currentLevel.ballsController.onComplete += OnLevelComplete;
        _currentLevel.ballsController.onAllBallsRemoved += OnAllBallsRemoved;

        _currentLevel.flowController.onLevelReachedDeadEnd += OnLevelReachedDeadEnd;

        if (_currentLevel.confettiVFX.activeInHierarchy)
        {
            _currentLevel.confettiVFX.SetActive(false);
        }

        _currentLevel.ballsController.onBallsFinishedSpawning += OnBallsFinishedSpawning;

        _screenController.ShowScreen<LevelScreen>();
        var ballsController = _currentLevel.ballsController;
        var levelScreen = _screenController.GetScreen<LevelScreen>();
        levelScreen.Setup(_currentLevelNumber, ballsController);
        levelScreen.onRestartButtonPressed += OnRestartButtonPressed;

        _state = State.Playing;

        if (_firstGameplay)
        {
            _firstGameplay = false;
            DefaultAnalytics.GameplayStarted();
        }

        DefaultAnalytics.LevelStarted(_currentLevelNumber);
    }

    void OnBallsFinishedSpawning()
    {
        _sliceController.ActivateWithLevel(_currentLevel);
        _touchTrailRender.SetActive(true);
    }

    void OnLevelComplete()
    {
        DefaultAnalytics.LevelCompleted();
        _state = State.WaitingToFillCup;
    }

    void OnAllBallsRemoved()
    {
        ApplyLose();
    }

    void ApplyLose()
    {
        if (_state != State.Playing)
        {
            return;
        }
        DefaultAnalytics.LevelFailed();
        _state = State.LevelLose;
        CoroutineUtils.ExecuteWithDelay(this, 1f, () =>
        {
            _screenController.ShowScreen<LevelLoseScreen>();
            var loseScreen = _screenController.GetScreen<LevelLoseScreen>();
            loseScreen.Setup(_currentLevelNumber);
        });
    }

    void OnLevelReachedDeadEnd()
    {
        ApplyLose();
    }

    void OnRestartButtonPressed()
    {
        ReloadScene();
    }

    LevelRoot GetLevelRootInScene(Scene scene)
    {
        var rootGameObjects = scene.GetRootGameObjects();
        foreach (var go in rootGameObjects)
        {
            if (go.TryGetComponent(out LevelRoot levelRoot))
            {
                return levelRoot;
            }
        }
        return null;
    }

}
